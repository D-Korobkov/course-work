package api.internal

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
import org.scalatest.wordspec.AnyWordSpec


class GithubApiTest extends AnyWordSpec with Matchers with ScalatestRouteTest {
  implicit def default(implicit system: ActorSystem): RouteTestTimeout = RouteTestTimeout(5.seconds)

  val routes: Route = TestApi.allRoutes

  "GithubApi" should {

    "unauthorized users have not access" in {
      Get("/auth/github/init") ~> routes ~> check {
        status shouldBe StatusCodes.Forbidden
      }
      Get("/auth/github/init").addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldBe StatusCodes.Forbidden
      }
    }

    "authorized users will get a link to github oAuth page" in {
      Get("/auth/github/init").addHeader(RawHeader("Authorization", "freshToken")) ~>
        routes ~> check {
        status shouldBe StatusCodes.OK
      }
    }
  }
}
