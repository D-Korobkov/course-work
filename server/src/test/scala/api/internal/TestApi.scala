package api.internal

import java.util.concurrent.{Executors, ScheduledExecutorService}

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteConcatenation._
import akka.http.scaladsl.testkit.RouteTestTimeout
import api.external.codeforces.CodeforcesApi
import api.external.github.GithubApi
import api.external.strava.StravaApi
import api.internal.endpoints._
import db.DatabaseInitiator._
import services._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

object TestApi {
  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = ac.dispatcher
  implicit val scheduler: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()

  implicit def default(implicit system: ActorSystem): RouteTestTimeout = RouteTestTimeout(5.seconds)

  implicit val cfExternalApi: CodeforcesApi = new CodeforcesApi()
  implicit val stravaExternalApi: StravaApi = new StravaApi()
  implicit val ghExternalApi: GithubApi = new GithubApi()

  val userService = new UserService()
  val noteService = new UserNotesService()
  val codeforcesService = new CodeforcesUserService()
  val githubService = new GithubUserService()
  val stravaService = new StravaUserService()

  val userApi = new UserApi(userService)
  val diaryApi = new DiaryApi(noteService)
  val userCodeforcesApi: CodeforcesUserApi = new CodeforcesUserApi(codeforcesService)
  val userGithubApi = new GithubUserApi(githubService)
  val userStravaApi = new StravaUserApi(stravaService)

  val userPublicRoutes: Seq[Route] = userApi.userPublicApi
  val userGithubPublicRoutes: Seq[Route] = userGithubApi.githubPublicApi
  val userStravaPublicRoutes: Seq[Route] = userStravaApi.stravaPublicApi

  val userPrivateRoutes: Seq[String => Route] = userApi.userPrivateApi
  val userDiaryPrivateRoutes: Seq[String => Route] = diaryApi.privateDiaryApi
  val userCodeforcesPrivateRoutes: Seq[String => Route] = userCodeforcesApi.codeforcesPrivateApi
  val userGithubPrivateRoutes: Seq[String => Route] = userGithubApi.githubPrivateApi
  val userStravaPrivateRoutes: Seq[String => Route] = userStravaApi.stravaPrivateApi

  val api = new Api(userService)
  val publicApi: Route = api.publicApi(userPublicRoutes ++ userGithubPublicRoutes ++ userStravaPublicRoutes)
  val privateApi: Route = api.privateApi {
    userPrivateRoutes ++ userCodeforcesPrivateRoutes ++ userGithubPrivateRoutes ++ userStravaPrivateRoutes ++ userDiaryPrivateRoutes
  }

  val allRoutes: Route = Route.seal(publicApi ~ privateApi)(exceptionHandler = LogLifeExceptionHandler.exceptionHandler)
}
