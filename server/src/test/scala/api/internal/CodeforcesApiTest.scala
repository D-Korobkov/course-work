package api.internal

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
import org.scalatest.wordspec.AnyWordSpec

class CodeforcesApiTest extends AnyWordSpec with Matchers with ScalatestRouteTest {
  implicit def default(implicit system: ActorSystem): RouteTestTimeout = RouteTestTimeout(5.seconds) // стороннее API... ничего не поделать
  val routes: Route = TestApi.allRoutes

  "CodeforcesApi" should {

    "unauthorized users have not access" in {
      Post("/auth/codeforces/init?nickname=Fefer_Ivan") ~> routes ~> check {
        status shouldBe StatusCodes.Forbidden
      }
      Post("/auth/codeforces/init?nickname=Fefer_Ivan").addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldBe StatusCodes.Forbidden
      }
    }

    "authorized users will receive an error message if they give an invalid handle" in {
      Post("/auth/codeforces/init?nickname=menyaNeSuchestvuet").addHeader(RawHeader("Authorization", "freshToken")) ~>
        routes ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }

    "authorized users can include this service with the valid codeforces handle" in {
      Post("/auth/codeforces/init?nickname=Fefer_Ivan").addHeader(RawHeader("Authorization", "ivanToken")) ~>
        routes ~> check {
        println(responseAs[String])
        status shouldBe StatusCodes.OK
      }
    }

    "authorized users can not replace old codeforces handle" in {
      Post("/auth/codeforces/init?nickname=tourist").addHeader(RawHeader("Authorization", "ivanToken")) ~>
        routes ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }

    "unauthorized users can not get or collect rating change info" in {
      Post("/auth/codeforces/rating/update") ~> routes ~> check {
        status shouldBe StatusCodes.Forbidden
      }
      Post("/auth/codeforces/rating/update").addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldBe StatusCodes.Forbidden
      }
      Get("/auth/codeforces/rating") ~> routes ~> check {
        status shouldBe StatusCodes.Forbidden
      }
      Get("/auth/codeforces/rating").addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldBe StatusCodes.Forbidden
      }
    }

    "authorized users can parse rating change info during the period they did not do it" in {
      Post("/auth/codeforces/update").addHeader(RawHeader("Authorization", "ivanToken")) ~>
        routes ~> check {
        status shouldBe StatusCodes.OK
      }
    }

    "authorized users can see their rating change info" in { // но иногда нужно, время пока данные спарсятся, можно смотреть логи
      Thread.sleep(5000)
      Get("/auth/codeforces/rating").addHeader(RawHeader("Authorization", "ivanToken")) ~>
        routes ~> check {
        status shouldBe StatusCodes.OK
      }
    }
  }
}
