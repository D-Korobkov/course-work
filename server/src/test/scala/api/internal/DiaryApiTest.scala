package api.internal

import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpEntity, StatusCodes}
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import db.entities.notes.{CustomNote, StandardNote}
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class DiaryApiTest extends AnyWordSpec with Matchers with ScalatestRouteTest {
  val routes: Route = TestApi.allRoutes

  val fullStandardNote: String =
    """{"mood" : "10",
      |"productivity": "8",
      |"awakenTime": "1620633600",
      |"asleepTime": "1620684000",
      |"revenues": "1000",
      |"expenses": "1000"}""".stripMargin

  val notFullStandardNote: String = """{"productivity": "7", "revenues": "1000"}""".stripMargin

  "DiaryApi" should {
    "work: unauthorized users can not add standard notes" in {
      Post("/auth/standard/add", HttpEntity(`application/json`, fullStandardNote)) ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
      Post("/auth/standard/add", HttpEntity(`application/json`, notFullStandardNote))
        .addHeader(RawHeader("Authorization", "invalidToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
      Post("/auth/standard/add", HttpEntity(`application/json`, fullStandardNote))
        .addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
    }

    "work: authorized users can add and rewrite standard notes" in {
      Post("/auth/standard/add", HttpEntity(`application/json`, fullStandardNote))
        .addHeader(RawHeader("Authorization", "ivanToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
      Post("/auth/standard/add", HttpEntity(`application/json`, notFullStandardNote))
        .addHeader(RawHeader("Authorization", "ivanToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    val validCustomNote = """{"categoryName": "test", "text": "test"}"""
    val invalidCustomNote = """{"categoryName": "invalid", "text": "invalid"}"""

    "work: unauthorized users can not create custom notes" in {
      Post("/auth/custom/addNote", HttpEntity(`application/json`, validCustomNote)) ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
      Post("/auth/custom/addNote", HttpEntity(`application/json`, validCustomNote)).addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
    }

    "work: authorized users can create custom notes only in their own categories" in {
      Post("/auth/custom/add/test").addHeader(RawHeader("Authorization", "ivanToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
      Post("/auth/custom/add/testCat").addHeader(RawHeader("Authorization", "ivanToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
      Post("/auth/custom/addNote", HttpEntity(`application/json`, validCustomNote))
        .addHeader(RawHeader("Authorization", "ivanToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
      Post("/auth/custom/addNote", HttpEntity(`application/json`, invalidCustomNote))
        .addHeader(RawHeader("Authorization", "ivanToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }

    val standardNote = StandardNote(1589148000, "userWithFreshToken", Some(3), Some(2), None, None, None, None)
    "work: authorized users can get their standard note at specified day" in {
      Get(s"/auth/standard/get/1589148000").addHeader(RawHeader("Authorization", "freshToken")) ~> routes ~> check {
        import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.unmarshaller
        implicit val jsonDecoder: Decoder[StandardNote] = deriveDecoder
        status shouldEqual StatusCodes.OK
        responseAs[StandardNote] shouldEqual standardNote
      }
    }

    val customNote = CustomNote(1589148000, "userWithFreshToken", "dogs", Some("puppy"))
    "work: authorized users can get their custom note in specified category at specified day" in {
      Get(s"/auth/custom/get/dogs/1589148000").addHeader(RawHeader("Authorization", "freshToken")) ~> routes ~> check {
        import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.unmarshaller
        implicit val jsonDecoder: Decoder[CustomNote] = deriveDecoder
        status shouldEqual StatusCodes.OK
        responseAs[CustomNote] shouldEqual customNote
      }
    }

    "work: unauthorized users can not see their diary" in {
      Get("/auth/diary") ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
    }

    "work: authorized users can see their diary" in {
      Get("/auth/diary").addHeader(RawHeader("Authorization", "ivanToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
    }
  }
}
