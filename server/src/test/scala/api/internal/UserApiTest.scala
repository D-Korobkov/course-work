package api.internal

import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpEntity, StatusCodes}
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import api.internal.entities.UserAvailableCategories
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec


class UserApiTest extends AnyWordSpec with Matchers with ScalatestRouteTest {
  val routes: Route = TestApi.allRoutes

  "UserApi" should {
    "return an access token in header for GET request to /signIn" in {
      Post("/signIn", HttpEntity(`application/json`, """{"username":"testUser1", "password":"12345"}""")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK

        val RawHeader(key, value) = headers.head
        key shouldEqual "Authorization"
        assert(value.nonEmpty)
      }
    }

    "return a BadRequest if credentials are invalid for GET request to /signIp" in {
      Post("/signIn", HttpEntity(`application/json`, """{"username":"invalid", "password":"credentials"}""")) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
      Post("/signIn", HttpEntity(`application/json`, """{"username":"invalid", "password":"12345"}""")) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
      Post("/signIn", HttpEntity(`application/json`, """{"username":"testUser1", "password":"credentials"}""")) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }

    "fail with exception if user with the given username exists for POST request to /signUp" in {
      Post("/signUp", HttpEntity(`application/json`, """{"username":"testUser1", "password":"qwe"}""")) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
      Post("/signUp", HttpEntity(`application/json`, """{"username":"testUser2", "password":"rty"}""")) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }

    "return a greeting for new user if the given username is free for POST request to /signUp" in {
      Post("/signUp", HttpEntity(`application/json`, """{"username":"newbie", "password":"peace"}""")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[String] shouldEqual "Hello, newbie!"
      }
    }

    "allow users to request an access token after registration" in {
      Post("/signUp", HttpEntity(`application/json`, """{"username":"super", "password":"man"}""")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }.andThen { _ =>
        Post("/signIn", HttpEntity(`application/json`, """{"username":"super", "password":"man"}""")) ~> routes ~> check {
          status shouldEqual StatusCodes.OK

          val RawHeader(key, value) = headers.head
          key shouldEqual "Authorization"
          assert(value.nonEmpty)
        }
      }
    }

    "check whether access token is fresh and return user name" in {
      Get("/auth").addHeader(RawHeader("Authorization", "freshToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[String] shouldEqual "userWithFreshToken"
      }
      Get("/auth").addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
    }

    "work: unauthorized users can not create custom categories" in {
      Post("/auth/custom/addCategory?categoryName=myCat") ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
      Post("/auth/custom/addCategory?categoryName=myCat").addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
    }

    "work: authorized users can create custom categories" in {
      Post("/auth/custom/add/myCat").addHeader(RawHeader("Authorization", "freshToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
      Post("/auth/custom/add/alsoMyCat").addHeader(RawHeader("Authorization", "freshToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    val userWithFreshTokenCats: UserAvailableCategories = UserAvailableCategories(
      custom = Seq("alsoMyCat", "myCat"), codeforces = false, github = false, strava = false
    )

    "return a json object which contains info about standard, custom and auto categories" in {
      Get("/auth/categories").addHeader(RawHeader("Authorization", "freshToken")) ~> routes ~> check {
        import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.unmarshaller
        status shouldEqual StatusCodes.OK
        responseAs[UserAvailableCategories] shouldEqual userWithFreshTokenCats
      }
      Get("/auth/categories").addHeader(RawHeader("Authorization", "oldToken")) ~> routes ~> check {
        status shouldEqual StatusCodes.Forbidden
      }
    }
  }
}
