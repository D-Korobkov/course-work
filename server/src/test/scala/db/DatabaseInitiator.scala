package db

import db.entities.User
import db.entities.notes.{CustomNote, StandardNote}
import db.entities.tokens.LogLifeToken
import db.repositories.UserQueryRepository
import db.repositories.notes._
import db.repositories.tokens._
import db.repositories.tokens.external.{CodeforcesQueryRepository, GithubQueryRepository, StravaQueryRepository}
import slick.jdbc.SQLiteProfile.api._

object DatabaseInitiator {
  implicit val db: Database = Database.forConfig("testDb")
  db.run(
    UserQueryRepository.AllUsers.schema.createIfNotExists >>

      UserQueryRepository.AllUsers.delete >>

      LogLifeQueryRepository.AllTokens.schema.createIfNotExists >>
      CodeforcesQueryRepository.AllTokens.schema.createIfNotExists >>
      GithubQueryRepository.AllTokens.schema.createIfNotExists >>
      StravaQueryRepository.AllTokens.schema.createIfNotExists >>

      LogLifeQueryRepository.AllTokens.delete >>
      CodeforcesQueryRepository.AllTokens.delete >>
      GithubQueryRepository.AllTokens.delete >>
      StravaQueryRepository.AllTokens.delete >>

      CodeforcesNotesRepository.AllNotes.schema.createIfNotExists >>
      GithubNotesRepository.AllNotes.schema.createIfNotExists >>
      StravaNotesRepository.AllNotes.schema.createIfNotExists >>
      StandardNotesRepository.AllNotes.schema.createIfNotExists >>
      CustomNotesRepository.AllNotes.schema.createIfNotExists >>

      CodeforcesNotesRepository.AllNotes.delete >>
      GithubNotesRepository.AllNotes.delete >>
      StravaNotesRepository.AllNotes.delete >>
      StandardNotesRepository.AllNotes.delete >>
      CustomNotesRepository.AllNotes.delete >>

      UserQueryRepository.addNewUser(User("testUser1", util.Security.hashPassword("12345").get)) >>
      UserQueryRepository.addNewUser(User("testUser2", util.Security.hashPassword("67890").get)) >>

      UserQueryRepository.addNewUser(User("userWithOldToken", util.Security.hashPassword("qwe").get)) >>
      UserQueryRepository.addNewUser(User("userWithFreshToken", util.Security.hashPassword("rty").get)) >>
      UserQueryRepository.addNewUser(User("ivan", util.Security.hashPassword("uio").get)) >>

      LogLifeQueryRepository.addToken(LogLifeToken("oldToken", "userWithOldToken", 1589148000)) >>
      LogLifeQueryRepository.addToken(LogLifeToken("freshToken", "userWithFreshToken", 1652220000)) >>
      LogLifeQueryRepository.addToken(LogLifeToken("ivanToken", "ivan", 1652220000)) >>

      StandardNotesRepository.addNote(StandardNote(1589148000, "userWithFreshToken", Some(3), Some(2), None, None, None, None)) >>
      CustomNotesRepository.addNote(CustomNote(1589148000, "userWithFreshToken", "dogs", Some("puppy")))
  )
}
