package db.repositories

import org.scalatest.matchers.should.Matchers
import db.repositories.UserQueryRepository._
import util.Security

class UserQueryRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getUserPassword should return hash of user password") {
    for {
      hashNeo <- getUserPassword("Neo")
      hashIvan <- getUserPassword("Ivan")
    } yield assert{
      hashNeo.isDefined && Security.verifyPassword("password", hashNeo.get).get &&
      hashIvan.isDefined && Security.verifyPassword("Ivanov", hashIvan.get).get
    }
  }

  test("getUserCodeforcesTokenId should return id of their codeforces token in other table") {
    for {
      tokenId <- getUserCodeforcesTokenId("Ivan")
    } yield tokenId shouldEqual Some(1)
  }

  test("getUserGithubTokenId should return id of their github token in other table") {
    for {
      tokenId <- getUserGithubTokenId("Ivan")
    } yield tokenId shouldEqual Some(1)
  }

  test("getUserStravaTokenId should return id of their strava token in other table") {
    for {
      tokenId <- getUserGithubTokenId("Ivan")
    } yield tokenId shouldEqual Some(1)
  }

  test("getUserCustomCategories should return list of user's custom categories") {
    for {
      categories <- getUserCustomCategories("Ivan")
    } yield categories should contain theSameElementsInOrderAs Seq("coffee cups", "tea cups")
  }

  test("getUserCategories should return info about all optional user's categories") {
    for {
      neoCategories <- getUserCategories("Neo")
      ivanCategories <- getUserCategories("Ivan")
    } yield {
      neoCategories shouldBe (Seq(""), false, false, false)
      ivanCategories shouldBe (Seq("coffee cups", "tea cups"), true, true, true)
    }
  }

  test("findUserByUserName should return user by its username") {
    for {
      stranger <- findUserByUserName("Rabbit")
      user <- findUserByUserName("Ivan")
    } yield assert(user.isDefined && stranger.isEmpty)
  }

  test("updateCustomCategories should add new custom categories") {
    for {
      _ <- updateCustomCategories("Neo", Seq("time in matrix"))
      neoCats <- getUserCategories("Neo")
    } yield neoCats shouldEqual (Seq("time in matrix"), false, false, false)
  }

  test("updateCodeforcesTokenId should add new id") {
    for {
      added <- updateCodeforcesTokenId("Neo", Some(2))
    } yield assert(added == 1)
  }

  test("updateGithubTokenId should add new id") {
    for {
      added <- updateGithubTokenId("Neo", Some(2))
    } yield assert(added == 1)
  }

  test("updateStravaTokenId should add new id") {
    for {
      added <- updateStravaTokenId("Neo", Some(2))
    } yield assert(added == 1)
  }

}
