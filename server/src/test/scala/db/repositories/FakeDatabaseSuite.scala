package db.repositories

import java.util.UUID

import db.repositories.notes._
import db.repositories.tokens.LogLifeQueryRepository
import db.repositories.tokens.external.{CodeforcesQueryRepository, GithubQueryRepository, StravaQueryRepository}
import org.scalactic.source
import org.scalatest.compatible
import org.scalatest.funsuite.AsyncFunSuite
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database
import db.entities._
import db.entities.tokens._
import db.entities.notes._
import util.Security

abstract class FakeDatabaseSuite extends AsyncFunSuite {
  protected def test[R, S <: NoStream, E <: Effect](testName: String)
                                                   (testFun: DBIOAction[compatible.Assertion, S, E])
                                                   (implicit pos: source.Position): Unit = {
    super.test(testName) {

      val db = Database.forURL(
        s"jdbc:h2:mem:${UUID.randomUUID()}",
        driver = "org.h2.Driver",
        keepAliveConnection = true
      )

      db.run(
        initSchema
          .andThen(UserQueryRepository.AllUsers ++= SampleUsers)
          .andThen(LogLifeQueryRepository.AllTokens ++= SampleTokens)
          .andThen(CodeforcesQueryRepository.AllTokens ++= SampleCodeforcesTokens)
          .andThen(GithubQueryRepository.AllTokens ++= SampleGithubTokens)
          .andThen(StravaQueryRepository.AllTokens ++= SampleStravaTokens)
          .andThen(StandardNotesRepository.AllNotes ++= SampleStandardNotes)
          .andThen(CustomNotesRepository.AllNotes ++= SampleCustomNotes)
          .andThen(CodeforcesNotesRepository.AllNotes ++= SampleCodeforcesNotes)
          .andThen(GithubNotesRepository.AllNotes ++= SampleGithubNotes)
          .andThen(StravaNotesRepository.AllNotes ++= SampleStravaNotes)
      )
        .flatMap(_ => db.run(testFun))
        .andThen { case _ => db.close() }
    }
  }

  private val initSchema = (UserQueryRepository.AllUsers.schema ++ LogLifeQueryRepository.AllTokens.schema ++
      CodeforcesQueryRepository.AllTokens.schema ++ GithubQueryRepository.AllTokens.schema ++
      StravaQueryRepository.AllTokens.schema ++ CodeforcesNotesRepository.AllNotes.schema ++
      GithubNotesRepository.AllNotes.schema ++ StravaNotesRepository.AllNotes.schema ++
      StandardNotesRepository.AllNotes.schema ++ CustomNotesRepository.AllNotes.schema).create
  
  protected val farFuture = 1740960000
  protected val sunday = 1621123200
  protected val monday = 1621209600
  protected val neoAwakenTime = 1621065600
  protected val ivanAsleepTime = 1621202400
  
  protected val SampleUsers = Seq(
    User("Neo", Security.hashPassword("password").get),
    User("Ivan", Security.hashPassword("Ivanov").get, Some(1), Some(1), Some(1), "coffee cups;tea cups")
  )
  
  protected val SampleTokens = Seq(
    LogLifeToken("neoToken", "Neo", farFuture),
    LogLifeToken("ivanToken", "Ivan", farFuture)
  )

  protected val SampleCodeforcesTokens = Seq(
    CodeforcesToken(Some(1), "handler")
  )

  protected val SampleGithubTokens = Seq(
    GithubToken(Some(1), "type", "token", "scope")
  )

  protected val SampleStravaTokens = Seq(
    StravaToken(Some(1), "type", "token", "refresh", farFuture, farFuture)
  )

  protected val SampleStandardNotes = Seq(
    StandardNote(monday, "Neo", Some(10), Some(8), None, None, Some(100), Some(50)),
    StandardNote(sunday, "Neo", Some(5), Some(9), Some(neoAwakenTime), None, None, Some(50)),
    StandardNote(monday, "Ivan", Some(5), Some(10), None, Some(ivanAsleepTime), None, Some(50)),
  )

  protected val SampleCustomNotes = Seq(
    CustomNote(monday, "Ivan", "coffee cups", Some("42")),
    CustomNote(monday, "Ivan", "tea cups", Some("17"))
  )

  protected val SampleCodeforcesNotes = Seq(
    CodeforcesNote(monday, "Ivan", 10, 7),
    CodeforcesNote(sunday, "Ivan", -10, 2),
  )
  
  protected val SampleGithubNotes = Seq(
    GithubNote(sunday, "Ivan", "repo1", 3),
    GithubNote(monday, "Ivan", "repo2", 0),
    GithubNote(sunday, "Ivan", "repo2", 2),
    GithubNote(monday, "Ivan", "repo1", 1),
  )

  protected val SampleStravaNotes = Seq(
    StravaNote(monday, "Ivan", "Run", 0.0),
    StravaNote(sunday, "Ivan", "Run", 1000.0),
  )
}
