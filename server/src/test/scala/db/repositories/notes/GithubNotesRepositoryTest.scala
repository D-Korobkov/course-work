package db.repositories.notes

import db.entities.notes.GithubNote
import db.repositories.FakeDatabaseSuite
import db.repositories.notes.GithubNotesRepository._
import org.scalatest.matchers.should.Matchers

class GithubNotesRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getAllUsersNotes should return all github notes") {
    for {
      ivanNotes <- getAllUsersNotes("Ivan")
    } yield ivanNotes should contain theSameElementsAs SampleGithubNotes.filter(_.ownerName == "Ivan")
  }

  test("getSortedAllUsersNotes should return all github notes ordered by day in ascending order") {
    implicit val githubNoteOrder: Ordering[GithubNote] = Ordering[Long].on[GithubNote](_.day)
    for {
      ivanNotes <- getSortedAllUsersNotes("Ivan")
    } yield {
      ivanNotes shouldBe sorted
      ivanNotes should contain theSameElementsAs SampleGithubNotes.filter(_.ownerName == "Ivan")
    }
  }

  test("getUserNotesInSpecifiedDay should return all github notes made in spec day") {
    for {
      ivanNotes <- getUserNotesInSpecifiedDay("Ivan", monday)
    } yield {
      ivanNotes should contain theSameElementsAs SampleGithubNotes.filter(x => x.ownerName == "Ivan" && x.day == monday)
    }
  }
}