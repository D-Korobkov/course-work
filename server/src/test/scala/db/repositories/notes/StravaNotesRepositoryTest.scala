package db.repositories.notes

import db.entities.notes.StravaNote
import db.repositories.FakeDatabaseSuite
import db.repositories.notes.StravaNotesRepository._
import org.scalatest.matchers.should.Matchers

class StravaNotesRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getAllUsersNotes should return all strava notes") {
    for {
      ivanNotes <- getAllUsersNotes("Ivan")
    } yield ivanNotes should contain theSameElementsAs SampleStravaNotes.filter(_.ownerName == "Ivan")
  }

  test("getSortedAllUsersNotes should return all strava notes ordered by day in ascending order") {
    implicit val stravaNoteOrder: Ordering[StravaNote] = Ordering[Long].on[StravaNote](_.day)
    for {
      ivanNotes <- getSortedAllUsersNotes("Ivan")
    } yield {
      ivanNotes shouldBe sorted
      ivanNotes should contain theSameElementsAs SampleStravaNotes.filter(_.ownerName == "Ivan")
    }
  }

  test("getUserNotesInSpecifiedDay should return all strava notes made in spec day") {
    for {
      ivanNotes <- getUserNotesInSpecifiedDay("Ivan", monday)
    } yield {
      ivanNotes should contain theSameElementsAs SampleStravaNotes.filter(x => x.ownerName == "Ivan" && x.day == monday)
    }
  }
}
