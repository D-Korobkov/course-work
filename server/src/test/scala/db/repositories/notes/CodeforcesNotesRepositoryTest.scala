package db.repositories.notes

import db.entities.notes.CodeforcesNote
import db.repositories.FakeDatabaseSuite
import db.repositories.notes.CodeforcesNotesRepository._
import org.scalatest.matchers.should.Matchers

class CodeforcesNotesRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getAllUsersNotes should return all codeforces notes") {
    for {
      ivanNotes <- getAllUsersNotes("Ivan")
    } yield ivanNotes should contain theSameElementsAs SampleCodeforcesNotes.filter(_.ownerName == "Ivan")
  }

  test("getSortedAllUsersNotes should return all codeforces notes ordered by day in ascending order") {
    implicit val codeforcesNoteOrder: Ordering[CodeforcesNote] = Ordering[Long].on[CodeforcesNote](_.day)
    for {
      ivanNotes <- getSortedAllUsersNotes("Ivan")
    } yield {
      ivanNotes shouldBe sorted
      ivanNotes should contain theSameElementsAs SampleCodeforcesNotes.filter(_.ownerName == "Ivan")
    }
  }

  test("getUserNotesInSpecifiedDay should return all codeforces notes made in spec day") {
    for {
      ivanNotes <- getUserNotesInSpecifiedDay("Ivan", monday)
    } yield {
      ivanNotes should contain theSameElementsAs SampleCodeforcesNotes.filter(x => x.ownerName == "Ivan" && x.day == monday)
    }
  }
}