package db.repositories.notes

import db.entities.notes.CustomNote
import db.repositories.FakeDatabaseSuite
import db.repositories.notes.CustomNotesRepository._
import org.scalatest.matchers.should.Matchers

class CustomNotesRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getAllUsersNotes should return all custom notes") {
    for {
      ivanNotes <- getAllUsersNotes("Ivan")
    } yield ivanNotes should contain theSameElementsAs SampleCustomNotes.filter(_.ownerName == "Ivan")
  }

  test("getSortedAllUsersNotes should return all custom notes ordered by day in ascending order") {
    implicit val customNoteOrder: Ordering[CustomNote] = Ordering[Long].on[CustomNote](_.day)
    for {
      ivanNotes <- getSortedAllUsersNotes("Ivan")
    } yield {
      ivanNotes shouldBe sorted
      ivanNotes should contain theSameElementsAs SampleCustomNotes.filter(_.ownerName == "Ivan")
    }
  }

  test("getUserNotesInSpecifiedDay should return all custom notes made in spec day") {
    for {
      ivanNotes <- getUserNotesInSpecifiedDay("Ivan", monday)
    } yield {
      ivanNotes should contain theSameElementsAs SampleCustomNotes.filter(x => x.ownerName == "Ivan" && x.day == monday)
    }
  }
  
  test("getUserNotesOfSpecifiedCategory should return all custom notes of spec category") {
    for {
      ivanNotes <- getUserNotesOfSpecifiedCategory("Ivan", "tea cups")
    } yield {
      ivanNotes should contain theSameElementsAs SampleCustomNotes.filter(_.categoryName == "tea cups")
    }
  }

  test("findSpecifiedCategoryNoteByDay should return a note which created at spec day in spec category") {
    for {
      ivanNote <- findSpecifiedCategoryNoteByDay("Ivan", "tea cups", monday)
    } yield {
      ivanNote shouldBe SampleCustomNotes.find(_.categoryName == "tea cups")
    }
  }

  test("replaceOldNote should rewrite an old note") {
    val newNote = CustomNote(monday, "Ivan", "coffee cups", None)
    for {
      _ <- replaceOldNote("Ivan", newNote)
      ivanNote <- findSpecifiedCategoryNoteByDay("Ivan", "coffee cups", monday)
    } yield {
      ivanNote shouldBe Some(newNote)
    }
  }
}
