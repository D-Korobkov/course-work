package db.repositories.notes

import db.entities.notes.StandardNote
import db.repositories.FakeDatabaseSuite
import db.repositories.notes.StandardNotesRepository._
import org.scalatest.matchers.should.Matchers

class StandardNotesRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getAllUsersNotes should return all standard notes") {
    for {
      neoNotes <- getAllUsersNotes("Neo")
      ivanNotes <- getAllUsersNotes("Ivan")
    } yield {
      neoNotes should contain theSameElementsAs  SampleStandardNotes.filter(_.ownerName == "Neo")
      ivanNotes should contain theSameElementsAs  SampleStandardNotes.filter(_.ownerName == "Ivan")
    }
  }

  test("getSortedAllUsersNotes should return all standard notes ordered by day in ascending order") {
    implicit val standardNoteOrder: Ordering[StandardNote] = Ordering[Long].on[StandardNote](_.day)
    for {
      neoNotes <- getSortedAllUsersNotes("Neo")
    } yield {
      neoNotes shouldBe sorted
      neoNotes should contain theSameElementsAs  SampleStandardNotes.filter(_.ownerName == "Neo")
    }
  }

  test("getUserNotesInSpecifiedDay should return all standard notes made in spec day") {
    for {
      neoNotes <- getUserNotesInSpecifiedDay("Neo", sunday)
    } yield {
      neoNotes should contain theSameElementsAs  SampleStandardNotes.filter(x => x.ownerName == "Neo" && x.day == sunday)
    }
  }

  test("replaceOldNote should replace old note by the new one") {
    val newNote = StandardNote(monday, "Ivan", None, None, None, None, None, None)
    for {
      _ <- replaceOldNote("Ivan", newNote)
      note <- getUserNotesInSpecifiedDay("Ivan", monday)
    } yield note.headOption shouldBe Some(newNote)
  }
}
