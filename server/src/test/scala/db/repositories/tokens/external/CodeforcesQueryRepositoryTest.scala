package db.repositories.tokens.external

import db.repositories.FakeDatabaseSuite
import db.repositories.tokens.external.CodeforcesQueryRepository._
import org.scalatest.matchers.should.Matchers

class CodeforcesQueryRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getIdByAccessToken should return token id by its value") {
    for {
      id <- getIdByAccessToken("handler")
    } yield id shouldBe SampleCodeforcesTokens.find(_.handle == "handler").flatMap(_.tokenId)
  }

  test("getAccessTokenById should return token by its id") {
    for {
      token <- getAccessTokenById(1)
    } yield token shouldBe SampleCodeforcesTokens.find(_.tokenId.contains(1)).map(_.handle)
  }

  test("getLastUpdatedByToken should return last day codeforces notes were updated") {
    for {
      day <- getLastUpdatedByToken("handler")
    } yield day shouldBe SampleCodeforcesTokens.find(_.handle == "handler").map(_.lastUpdated)
  }

  test("updateLastUpdatedByToken should update last day codeforces notes were updated") {
    for {
      _ <- updateLastUpdatedByToken("handler", sunday)
      day <- getLastUpdatedByToken("handler")
    } yield day shouldBe Some(sunday)
  }
}
