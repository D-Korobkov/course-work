package db.repositories.tokens.external

import db.repositories.FakeDatabaseSuite
import db.repositories.tokens.external.GithubQueryRepository._
import org.scalatest.matchers.should.Matchers

class GithubQueryRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getIdByAccessToken should return token id by its value") {
    for {
      id <- getIdByAccessToken("token")
    } yield id shouldBe SampleGithubTokens.find(_.accessToken == "token").flatMap(_.tokenId)
  }

  test("getAccessTokenById should return token by its id") {
    for {
      token <- getAccessTokenById(1)
    } yield token shouldBe SampleGithubTokens.find(_.tokenId.contains(1)).map(_.accessToken)
  }

  test("getLastUpdatedByToken should return last day github notes were updated") {
    for {
      day <- getLastUpdatedByToken("token")
    } yield day shouldBe SampleGithubTokens.find(_.accessToken == "token").map(_.lastUpdated)
  }

  test("updateLastUpdatedByToken should update last day github notes were updated") {
    for {
      _ <- updateLastUpdatedByToken("token", sunday)
      day <- getLastUpdatedByToken("token")
    } yield day shouldBe Some(sunday)
  }
}
