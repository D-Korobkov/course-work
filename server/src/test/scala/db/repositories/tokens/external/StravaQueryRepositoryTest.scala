package db.repositories.tokens.external

import db.repositories.FakeDatabaseSuite
import db.repositories.tokens.external.StravaQueryRepository._
import org.scalatest.matchers.should.Matchers

class StravaQueryRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getIdByAccessToken should return token id by its value") {
    for {
      id <- getIdByAccessToken("token")
    } yield id shouldBe SampleStravaTokens.find(_.accessToken == "token").flatMap(_.tokenId)
  }

  test("getAccessTokenById should return token by its id") {
    for {
      token <- getAccessTokenById(1)
    } yield token shouldBe SampleStravaTokens.find(_.tokenId.contains(1)).map(_.accessToken)
  }

  test("getRefreshTokenById should return refresh token by its id") {
    for {
      refreshToken <- getRefreshTokenById(1)
    } yield refreshToken shouldBe SampleStravaTokens.find(_.tokenId.contains(1)).map(_.refreshToken)
  }

  test("getTokenExpiresAtTime should return time when token will expire") {
    for {
      expAt <- getTokenExpiresAtTime(1)
    } yield expAt shouldBe SampleStravaTokens.find(_.tokenId.contains(1)).map(_.expiresAt)
  }

  test("getLastUpdatedByToken should return last day strava notes were updated") {
    for {
      day <- getLastUpdatedByToken("token")
    } yield day shouldBe SampleStravaTokens.find(_.accessToken == "token").map(_.lastUpdated)
  }

  test("updateLastUpdatedByToken should update last day strava notes were updated") {
    for {
      _ <- updateLastUpdatedByToken("token", sunday)
      day <- getLastUpdatedByToken("token")
    } yield day shouldBe Some(sunday)
  }
}

