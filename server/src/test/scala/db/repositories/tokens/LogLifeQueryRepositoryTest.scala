package db.repositories.tokens

import db.repositories.FakeDatabaseSuite
import org.scalatest.matchers.should.Matchers
import db.repositories.tokens.LogLifeQueryRepository._

class LogLifeQueryRepositoryTest extends FakeDatabaseSuite with Matchers {
  test("getToken should return token") {
    for {
      token <- getToken("neoToken")
    } yield token shouldBe SampleTokens.find(_.token == "neoToken")
  }
}
