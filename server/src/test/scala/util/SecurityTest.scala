package util

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class SecurityTest extends AnyWordSpec with Matchers {
  "Security" should {
    "hash the given password" in {
      assert(Security.hashPassword("password").isSuccess)
    }
    "match the given password and hash" in {
      assert(Security.verifyPassword("password", Security.hashPassword("password").get).get)
      assert(!Security.verifyPassword("Password", Security.hashPassword("password").get).get)
    }
    "generate json web token" in {
      assert(Security.generateToken.nonEmpty)
    }
  }
}
