package util

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class TimeUtilTest extends AnyWordSpec with Matchers {
 "TimeUtil" should {
   val currentTime1: Long = 157766400L // 01 Jan 1975 00:00:00
   val dayStartTime1: Long = 157766400L // 01 Jan 1975 00:00:00
   val nextDayStartTime1: Long = 157852800L // 02 Jan 1975 00:00:00
   val currentTimeIso1: String = "1975-01-01T00:00:00Z"

   val currentTime2: Long = 1262311921L // 01 Jan 2010 02:12:01
   val dayStartTime2: Long = 1262304000L // 01 Jan 2010 00:00:00
   val nextDayStartTime2: Long = 1262390400L // 02 Jan 2010 00:00:00
   val currentTimeIso2: String = "2010-01-01T02:12:01Z"

   val currentTime3: Long = 1583193599L // 02 Mar 2020 23:59:59
   val dayStartTime3: Long = 1583107200L // 02 Mar 2020 00:00:00
   val nextDayStartTime3: Long = 1583193600L // 03 Mar 2020 00:00:00
   val currentTimeIso3: String = "2020-03-02T23:59:59Z"

   "return unix timestamp of the current day's start" in {
     TimeUtil.getBeginningOfCurrentDayInSeconds(currentTime1) shouldEqual dayStartTime1
     TimeUtil.getBeginningOfCurrentDayInSeconds(currentTime2) shouldEqual dayStartTime2
     TimeUtil.getBeginningOfCurrentDayInSeconds(currentTime3) shouldEqual dayStartTime3
   }

   "return unix timestamp of the next day's start" in {
     TimeUtil.getBeginningOfNextDayInSeconds(currentTime1) shouldEqual nextDayStartTime1
     TimeUtil.getBeginningOfNextDayInSeconds(currentTime2) shouldEqual nextDayStartTime2
     TimeUtil.getBeginningOfNextDayInSeconds(currentTime3) shouldEqual nextDayStartTime3
   }

   "convert unix timestamp to iso8601 format" in {
     TimeUtil.toISO8601UTC(currentTime1) shouldEqual currentTimeIso1
     TimeUtil.toISO8601UTC(currentTime2) shouldEqual currentTimeIso2
     TimeUtil.toISO8601UTC(currentTime3) shouldEqual currentTimeIso3
   }

   "convert iso8601 timestamp to unix format" in {
     TimeUtil.fromISO8601UTC(currentTimeIso1) shouldEqual currentTime1
     TimeUtil.fromISO8601UTC(currentTimeIso2) shouldEqual currentTime2
     TimeUtil.fromISO8601UTC(currentTimeIso3) shouldEqual currentTime3
   }
 }
}
