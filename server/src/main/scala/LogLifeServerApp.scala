import java.util.concurrent.{Executors, ScheduledExecutorService}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteConcatenation._
import api.external.codeforces.CodeforcesApi
import api.external.github.GithubApi
import api.external.strava.StravaApi
import api.internal._
import api.internal.endpoints._
import com.typesafe.scalalogging.LazyLogging
import migrations.FlywayMigrator
import services._
import slick.jdbc.SQLiteProfile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

object LogLifeServerApp {
  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = ac.dispatcher
  implicit val scheduler: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
  implicit val database: Database = Database.forConfig("loglife")

  val app: LogLifeApp = LogLifeApp()

  def main(args: Array[String]): Unit = {
    val appRoutes = app.buildRoutes

    val initMigrationTask = new FlywayMigrator(
      "server/src/main/resources/application.conf", "loglife"
    )

    Await.result(initMigrationTask.connectAndMigrate().flatMap(_ => app.start(appRoutes)), Duration.Inf)
  }

}

case class LogLifeApp()(implicit ac: ActorSystem, ec: ExecutionContext, scheduler: ScheduledExecutorService) extends LazyLogging {
  def buildRoutes(implicit db: Database): Route = {
    implicit val cfExternalApi: CodeforcesApi = new CodeforcesApi()
    implicit val stravaExternalApi: StravaApi = new StravaApi()
    implicit val ghExternalApi: GithubApi = new GithubApi()

    val userService = new UserService()
    val notesService = new UserNotesService()
    val codeforcesService = new CodeforcesUserService()
    val githubService = new GithubUserService()
    val stravaService = new StravaUserService()

    val userApi = new UserApi(userService)
    val diaryApi = new DiaryApi(notesService)
    val userCodeforcesApi: CodeforcesUserApi = new CodeforcesUserApi(codeforcesService)
    val userGithubApi = new GithubUserApi(githubService)
    val userStravaApi = new StravaUserApi(stravaService)

    val userPublicRoutes: Seq[Route] = userApi.userPublicApi
    val userGithubPublicRoutes: Seq[Route] = userGithubApi.githubPublicApi
    val userStravaPublicRoutes: Seq[Route] = userStravaApi.stravaPublicApi

    val userPrivateRoutes: Seq[String => Route] = userApi.userPrivateApi
    val userDiaryPrivateRoutes: Seq[String => Route] = diaryApi.privateDiaryApi
    val userCodeforcesPrivateRoutes: Seq[String => Route] = userCodeforcesApi.codeforcesPrivateApi
    val userGithubPrivateRoutes: Seq[String => Route] = userGithubApi.githubPrivateApi
    val userStravaPrivateRoutes: Seq[String => Route] = userStravaApi.stravaPrivateApi

    val webSitePublicRoutes = WebSiteApi.publicApi
    val webSitePrivateRoutes = WebSiteApi.privateApi

    val api = new Api(userService)
    val publicApi: Route = api.publicApi(
      userPublicRoutes ++ userGithubPublicRoutes ++ userStravaPublicRoutes ++ webSitePublicRoutes
    )
    val privateApi: Route = api.privateApi {
      userPrivateRoutes ++ userCodeforcesPrivateRoutes ++ userGithubPrivateRoutes ++ userStravaPrivateRoutes ++
        userDiaryPrivateRoutes ++ webSitePrivateRoutes
    }

    Route.seal(publicApi ~ privateApi)(exceptionHandler = LogLifeExceptionHandler.exceptionHandler)
  }

  def start(routes: Route): Future[Http.ServerBinding] = {
    Http()
      .newServerAt("localhost", 8080)
      .bind(routes)
      .andThen { case b => logger.info(s"server started at: $b") }
  }
}