package api.internal

import java.io.File

import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{AuthorizationFailedRejection, Route}


object WebSiteApi {
  private def staticResources: Route = pathPrefix("static") {
    getFromDirectory("public/static")
  }

  private def pages: Route = path("site" | "") {
    pathEndOrSingleSlash {
      getFromFile(new File(s"public/index.html"), ContentTypes.`text/html(UTF-8)`)
    }
  } ~ path("site" / Segment) { file =>
    getFromFile(new File(s"public/$file.html"), ContentTypes.`text/html(UTF-8)`)
  }

  private def fileLink(userName: String): Route = path("private" / Segment) { fileName: String =>
    get {
      println(fileName)
      if (s"$userName.csv" == fileName) {
        getFromFile(new java.io.File(s"private/$fileName"), ContentTypes.`text/csv(UTF-8)`)
      } else {
        reject(AuthorizationFailedRejection)
      }
    }
  }

  def publicApi: Seq[Route] = Seq(pages, staticResources)

  def privateApi: Seq[String => Route] = Seq(fileLink)
}
