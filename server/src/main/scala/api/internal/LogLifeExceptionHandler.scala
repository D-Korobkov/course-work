package api.internal

import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler
import util.LogLifeException

object LogLifeExceptionHandler {
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val exceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case definedError: LogLifeException => complete(BadRequest, ExceptionResponse(definedError.getMessage))
      case undefinedError: Throwable => complete(BadRequest, ExceptionResponse("Server is relaxing... " + undefinedError.getMessage))
    }
}
