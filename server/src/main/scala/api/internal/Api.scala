package api.internal

import akka.http.scaladsl.server.Directives.{onSuccess, optionalHeaderValueByName, pathPrefix, provide, reject}
import akka.http.scaladsl.server.RouteConcatenation._
import akka.http.scaladsl.server.{AuthorizationFailedRejection, Directive1, Route}
import services.UserService

class Api(basicUserService: UserService) {
  def publicApi(routes: Seq[Route]): Route = {
    routes.foldLeft[Route](reject)(_ ~ _)
  }

  def privateApi(userDependedRoutes: Seq[String => Route]): Route = {
    pathPrefix("auth") {
      authenticated { userName =>
        userDependedRoutes.map(route => route(userName)).foldLeft[Route](reject)(_ ~ _)
      }
    }
  }

  private def authenticated: Directive1[String] = {
    optionalHeaderValueByName("Authorization").flatMap {
      case Some(token) =>
        onSuccess(basicUserService.checkAuthentication(token)).flatMap {
          case Some(userName) => provide(userName)
          case None => reject(AuthorizationFailedRejection)
        }
      case None => reject(AuthorizationFailedRejection)
    }
  }
}
