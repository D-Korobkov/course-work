package api.internal.endpoints

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.jsonMarshaller
import services.GithubUserService

import scala.util.{Failure, Success}

class GithubUserApi(githubUserService: GithubUserService) {

  private def initService(userName: String): Route = path("github" / "init") {
    get {
      val gettingLink = githubUserService.getRedirectLink(userName)
      onComplete(gettingLink) {
        case Success(link) => complete(link.toString())
        case Failure(exception) => failWith(exception)
      }
    }
  }

  private def completeOauth: Route = path("github" / "oauth" / Segment) { userName =>
    get {
      parameter("code") { sessionCode =>
        onSuccess(githubUserService.completeOauth(userName, sessionCode)) { success =>
          if (success) {
            redirect(s"http://localhost:8080/site/settings", StatusCodes.PermanentRedirect)
          } else
            redirect(s"http://localhost:8080/site/settings/fail", StatusCodes.PermanentRedirect)
        }
      }
    }
  }

  private def updateNotes(userName: String): Route = path("github" / "update") {
    post {
      githubUserService.fillGithubNotes(userName)
      complete(StatusCodes.OK)
    }
  }

  private def getGithubNotes(userName: String): Route = path("github" / "notes") {
    get {
      complete(githubUserService.getAllNotesAsJson(userName))
    }
  }

  def githubPublicApi: Seq[Route] = Seq(completeOauth)
  def githubPrivateApi: Seq[String => Route] = Seq(initService, getGithubNotes, updateNotes)
}
