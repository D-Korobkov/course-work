package api.internal.endpoints

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import api.internal.entities.{UserCustomNote, UserStandardNote}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.{jsonMarshaller, unmarshaller}
import services.UserNotesService
import util.DatabaseException

import scala.util.{Failure, Success}


class DiaryApi(val userNotesService: UserNotesService) {
  private def addStandardNote(userName: String): Route = path("standard" / "add") { //
    post {
      entity(as[UserStandardNote]) { note =>
        onSuccess(userNotesService.addNewStandardNote(userName, note)) {
          case true => complete(HttpResponse(StatusCodes.OK))
          case false => failWith(DatabaseException())
        }
      }
    }
  }

  private def addCustomNote(userName: String): Route = path("custom" / "addNote") { //
    post {
      entity(as[UserCustomNote]) { note =>
        val addingNote = userNotesService.addNewCustomNote(userName, note)
        onComplete(addingNote) {
          case Success(true) => complete(HttpResponse(StatusCodes.OK))
          case Success(false) => failWith(DatabaseException())
          case Failure(exception) => failWith(exception)
        }
      }
    }
  }

  private def getStandardNoteAtSpecifiedDay(userName: String): Route = {
    path("standard" / "get" / LongNumber) { day =>
      get {
        complete(userNotesService.getSpecifiedDayStandardNoteAsJson(userName, day))
      }
    }
  }

  private def getCustomNoteAtSpecifiedDay(userName: String): Route = {
    path("custom" / "get" / Segment / LongNumber) { (catName, day) =>
      get {
        complete(userNotesService.getSpecifiedDayCustomNoteAsJson(userName, catName, day))
      }
    }
  }

  private def showUsersDiary(userName: String): Route = path("diary") {
    get {
      complete(userNotesService.getAllNotesAsJson(userName))
    }
  }

  private def showSpecifiedDiaryPage(userName: String): Route = path("diary" / "get" / LongNumber) { day =>
    get {
      complete(userNotesService.getSpecifiedDayNoteAsJson(userName, day))
    }
  }

  private def downloadDiary(userName: String): Route = path("diary" / "download") {
    post {
      complete(userNotesService.createFileWithUserNotes(userName))
    }
  }

  def privateDiaryApi: Seq[String => Route] = Seq(
    addStandardNote, addCustomNote, getStandardNoteAtSpecifiedDay, getCustomNoteAtSpecifiedDay,
    showUsersDiary, showSpecifiedDiaryPage, downloadDiary
  )
}
