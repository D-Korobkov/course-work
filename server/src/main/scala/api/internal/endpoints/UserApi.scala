package api.internal.endpoints

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import api.internal.entities.LoginRequest
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.{jsonMarshaller, unmarshaller}
import io.circe.generic.auto.exportDecoder
import services.UserService
import util.{DatabaseException, InvalidCredentialsException}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class UserApi(userService: UserService)(implicit ex: ExecutionContext) {
  private def signUpUser: Route = path("signUp") {
    post {
      entity(as[LoginRequest]) { credentials =>
        val registration = userService.registerUser(credentials)

        onComplete(registration) {
          case Success(userName) => complete(s"Hello, $userName!")
          case Failure(error) => failWith(error)
        }
      }
    }
  }

  private def signInUser: Route = path("signIn") {
    post {
      entity(as[LoginRequest]) { credentials =>
        val tokenCreation: Future[String] = {
          userService.verifyUserSecrets(credentials).flatMap { verified =>
            if (verified)
              userService.createAccessToken(credentials.username)
            else
              Future.failed(InvalidCredentialsException())
          }
        }

        onComplete(tokenCreation) {
          case Success(token) =>
            respondWithHeader(RawHeader("Authorization", token)) {
              complete(StatusCodes.OK)
            }
          case Failure(error) => failWith(error)
        }
      }
    }
  }

  private def validateAccessToken(userName: String): Route = pathEndOrSingleSlash {
    get {
      complete(userName)
    }
  }

  private def getUserCategories(userName: String): Route = path("categories") {
    get {
      complete(userService.getAllUsersCategories(userName))
    }
  }

  private def addCustomCategory(userName: String): Route = path("custom" / "add" / Segment) { categoryName =>
    post {
      val adding = userService.addCustomCategory(userName, categoryName)
      onComplete(adding) {
        case Success(1) => complete(HttpResponse(StatusCodes.OK))
        case Failure(error) => failWith(error)
        case _ => failWith(DatabaseException())
      }
    }
  }

  def userPublicApi: Seq[Route] = Seq(signUpUser, signInUser)

  def userPrivateApi: Seq[String => Route] = Seq(addCustomCategory, validateAccessToken, getUserCategories)
}
