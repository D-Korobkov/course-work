package api.internal.endpoints

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.jsonMarshaller
import services.StravaUserService

import scala.util.{Failure, Success}

class StravaUserApi(stravaUserService: StravaUserService) {

  private def initService(userName: String): Route = path("strava" / "init") {
    get {
      val gettingLink = stravaUserService.getRedirectLink(userName)
      onComplete(gettingLink) {
        case Success(link) => complete(link.toString())
        case Failure(exception) => failWith(exception)
      }
    }
  }

  private def completeOauth: Route = path("strava" / "oauth" / Segment) { userName =>
    get {
      parameter("code") { sessionCode =>
        onSuccess(stravaUserService.completeOauth(userName, sessionCode)) { success =>
          if (success)
            redirect(s"http://localhost:8080/site/settings", StatusCodes.PermanentRedirect)
          else
            redirect(s"http://localhost:8080/site/settings/fail", StatusCodes.PermanentRedirect)
        }
      }
    }
  }

  private def updateNotes(userName: String): Route = path("strava" / "update") {
    post {
      stravaUserService.fillStravaNotes(userName)
      complete(HttpResponse(StatusCodes.OK))
    }
  }

  private def getStravaNotes(userName: String): Route = path("strava" / "notes") {
    get {
      complete(stravaUserService.getAllNotesAsJson(userName))
    }
  }

  def stravaPublicApi: Seq[Route] = Seq(completeOauth)
  def stravaPrivateApi: Seq[String => Route] = Seq(initService, updateNotes, getStravaNotes)
}
