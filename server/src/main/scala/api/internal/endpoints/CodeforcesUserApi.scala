package api.internal.endpoints

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.jsonMarshaller
import services.CodeforcesUserService
import util.DatabaseException

import scala.util.{Failure, Success}

class CodeforcesUserApi(codeforcesUserService: CodeforcesUserService) {
  private def initService(userName: String): Route = path("codeforces" / "init") {
    post {
      parameter("nickname") { nickname =>
        val gettingInfo = codeforcesUserService.getUserInfoByNickname(userName, nickname)
        onComplete(gettingInfo) {
          case Success(userInfo) =>
            val updatingHandle = codeforcesUserService.updateCodeforcesHandle(userName, userInfo)
            onComplete(updatingHandle) {
              case Success(true) => complete(HttpResponse(StatusCodes.OK))
              case _ => failWith(DatabaseException())
            }
          case Failure(exception) => failWith(exception)
        }
      }
    }
  }

  private def parseRatingChangeInfo(userName: String): Route = path("codeforces" / "update") {
    post {
      codeforcesUserService.fillCodeforcesNotes(userName)
      complete(StatusCodes.OK)
    }
  }

  private def getRatingChangeInfo(userName: String): Route = path("codeforces" / "rating") {
    get {
      complete(codeforcesUserService.getAllNotesAsJson(userName))
    }
  }

  def codeforcesPrivateApi: Seq[String => Route] = Seq(initService, parseRatingChangeInfo, getRatingChangeInfo)
}
