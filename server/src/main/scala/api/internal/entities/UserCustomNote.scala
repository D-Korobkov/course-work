package api.internal.entities

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class UserCustomNote(categoryName: String, text: Option[String])

object UserCustomNote {
  implicit val jsonDecoder: Decoder[UserCustomNote] = deriveDecoder
  implicit val jsonEncoder: Encoder[UserCustomNote] = deriveEncoder
}
