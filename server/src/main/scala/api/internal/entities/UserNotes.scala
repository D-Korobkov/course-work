package api.internal.entities

import db.entities.notes._

case class UserNotes(standardNotes: Seq[StandardNote],
                     customNotes: Seq[CustomNote],
                     codeforcesNotes: Seq[CodeforcesNote],
                     githubNotes: Seq[GithubNote],
                     stravaNotes: Seq[StravaNote])
