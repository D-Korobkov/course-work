package api.internal.entities

final case class LoginRequest(username: String, password: String)