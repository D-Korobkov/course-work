package api.internal.entities

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class UserStandardNote(mood: Option[Int],
                            productivity: Option[Int],
                            awakenTime: Option[Long],
                            asleepTime: Option[Long],
                            revenues: Option[Int],
                            expenses: Option[Int])
object UserStandardNote {
  implicit val jsonDecoder: Decoder[UserStandardNote] = deriveDecoder
  implicit val jsonEncoder: Encoder[UserStandardNote] = deriveEncoder
}
