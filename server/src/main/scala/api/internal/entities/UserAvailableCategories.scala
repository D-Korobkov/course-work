package api.internal.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class UserAvailableCategories(standard: Seq[String] = Seq("mood", "productivity", "awakenTime", "asleepTime", "revenues", "expenses"),
                                   custom: Seq[String],
                                   codeforces: Boolean,
                                   github: Boolean,
                                   strava: Boolean)

object UserAvailableCategories {
  implicit val jsonDecoder: Decoder[UserAvailableCategories] = deriveDecoder
  implicit val jsonEncoder: Encoder[UserAvailableCategories] = deriveEncoder
}