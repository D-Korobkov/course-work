package api.external.strava.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}


case class StravaUserToken(token_type: String,
                           expires_at: Long,
                           expires_in: Long,
                           refresh_token: String,
                           access_token: String)

object StravaUserToken {
  implicit val jsonDecoder: Decoder[StravaUserToken] = deriveDecoder
  implicit val jsonEncoder: Encoder[StravaUserToken] = deriveEncoder
}

