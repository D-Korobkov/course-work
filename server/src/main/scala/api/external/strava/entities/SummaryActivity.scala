package api.external.strava.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class SummaryActivity(distance: Double,
                          `type`: String,
                           start_date: String) // ISO 8601

object SummaryActivity {
  implicit val jsonDecoder: Decoder[SummaryActivity] = deriveDecoder
  implicit val jsonEncoder: Encoder[SummaryActivity] = deriveEncoder
}