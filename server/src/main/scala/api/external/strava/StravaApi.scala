package api.external.strava

import java.io.FileInputStream
import java.util.Properties

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.headers.RawHeader
import api.external.RequestBuilder
import api.external.strava.entities.{StravaUserToken, SummaryActivity}
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

import scala.concurrent.{ExecutionContext, Future}

class StravaApi(implicit ac: ActorSystem, ex: ExecutionContext) extends LazyLogging {
  private[this] val properties = new Properties
  properties.load(new FileInputStream("server/src/main/resources/strava-api.properties"))

  private[this] def getClientId: String = properties.getProperty("CLIENT_ID")

  private[this] def getClientSecret: String = properties.getProperty("CLIENT_SECRET")

  def getLinkToScalaAuthPage(redirectUri: String): Future[Uri] = Future {
    Uri("https://www.strava.com/oauth/authorize").withQuery(
      Query(
        "client_id" -> getClientId,
        "response_type" -> "code",
        "redirect_uri" -> redirectUri,
        "approval_prompt" -> "force",
        "scope" -> "activity:read_all"
      )
    )
  }

  def completeScalaOauth(sessionCode: String): Future[StravaUserToken] = {
    RequestBuilder.simplePost[StravaUserToken](
      Uri("https://www.strava.com/oauth/token").withQuery(
        Query(
          "client_id" -> getClientId,
          "client_secret" -> getClientSecret,
          "code" -> sessionCode,
          "grant_type" -> "authorization_code"
        )
      )
    )
  }

  def refreshAccessToken(refreshToken: String): Future[StravaUserToken] = {
    RequestBuilder.simplePost[StravaUserToken](
      Uri("https://www.strava.com/api/v3/oauth/token").withQuery(
        Query(
          "client_id" -> getClientId,
          "client_secret" -> getClientSecret,
          "grant_type" -> "refresh_token",
          "refresh_token" -> refreshToken
        )
      )
    )
  }

  def getUserActivities(token: String, after: Long, before: Long, page: Int): Future[List[SummaryActivity]] = {
    RequestBuilder.get[List[SummaryActivity]](
      Uri("https://www.strava.com/api/v3/athlete/activities").withQuery(
        Query(
          "before" -> before.toString,
          "after" -> after.toString,
          "per_page" -> "100",
          "page" -> page.toString
        )
      ),
      Seq(RawHeader("Authorization", s"Bearer $token"))
    )
  }
}
