package api.external.codeforces.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class SubmissionCollection(result: List[Submission])

object SubmissionCollection {
  implicit val jsonDecoder: Decoder[SubmissionCollection] = deriveDecoder
  implicit val jsonEncoder: Encoder[SubmissionCollection] = deriveEncoder
}