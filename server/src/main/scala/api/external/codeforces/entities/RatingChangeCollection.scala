package api.external.codeforces.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class RatingChangeCollection(result: List[RatingChange])

object RatingChangeCollection {
  implicit val jsonDecoder: Decoder[RatingChangeCollection] = deriveDecoder
  implicit val jsonEncoder: Encoder[RatingChangeCollection] = deriveEncoder
}
