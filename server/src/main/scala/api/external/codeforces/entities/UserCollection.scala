package api.external.codeforces.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class UserCollection(result: List[User])

object UserCollection {
  implicit val jsonDecoder: Decoder[UserCollection] = deriveDecoder
  implicit val jsonEncoder: Encoder[UserCollection] = deriveEncoder
}
