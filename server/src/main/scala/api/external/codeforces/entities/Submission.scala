package api.external.codeforces.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class Submission(creationTimeSeconds: Long,
                      verdict: Option[String])
object Submission {
  implicit val jsonDecoder: Decoder[Submission] = deriveDecoder
  implicit val jsonEncoder: Encoder[Submission] = deriveEncoder
}
