package api.external.codeforces.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class RatingChange(contestId: Int,
                        contestName: String,
                        handle: String,
                        rank: Int,
                        ratingUpdateTimeSeconds: Long,
                        oldRating: Int,
                        newRating: Int)
object RatingChange {
  implicit val jsonDecoder: Decoder[RatingChange] = deriveDecoder
  implicit val jsonEncoder: Encoder[RatingChange] = deriveEncoder
}
