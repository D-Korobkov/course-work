package api.external.codeforces

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.Uri.Query
import api.external.RequestBuilder
import api.external.codeforces.entities.{RatingChangeCollection, SubmissionCollection, UserCollection}
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

import scala.concurrent.{ExecutionContext, Future}

class CodeforcesApi(implicit ac: ActorSystem, ex: ExecutionContext) extends LazyLogging {
  def getRatingChangeInfo(userName: String): Future[RatingChangeCollection] = {
    RequestBuilder.simpleGet[RatingChangeCollection](
      Uri("https://codeforces.com/api/user.rating").withQuery(
        Query("handle" -> userName)
      )
    )
  }

  def getSubmissionsInfo(userName: String): Future[SubmissionCollection] = {
    RequestBuilder.simpleGet[SubmissionCollection](
      Uri("https://codeforces.com/api/user.status").withQuery(
        Query("handle" -> userName)
      )
    )
  }

  def getUsersInfo(users: List[String]): Future[UserCollection] = {
    RequestBuilder.simpleGet[UserCollection](
      Uri("https://codeforces.com/api/user.info").withQuery(
        Query("handles" -> users.mkString(";"))
      )
    )
  }
}
