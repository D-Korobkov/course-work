package api.external.github

import java.io.FileInputStream
import java.util.Properties

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.headers.RawHeader
import api.external.RequestBuilder
import api.external.github.entities.{CommitInfo, GithubUserToken, Repo}
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import util.TimeUtil.toISO8601UTC

import scala.concurrent.{ExecutionContext, Future}

class GithubApi(implicit ac: ActorSystem, ex: ExecutionContext) extends LazyLogging {
  private[this] val properties = new Properties
  properties.load(new FileInputStream("server/src/main/resources/github-api.properties"))

  private[this] def getClientId: String = properties.getProperty("CLIENT_ID")

  private[this] def getClientSecret: String = properties.getProperty("CLIENT_SECRET")

  def getLinkToGithubAuthPage(redirectedUri: String): Future[Uri] = Future {
    Uri("https://github.com/login/oauth/authorize").withQuery(
      Query(
        "client_id" -> getClientId,
        "redirect_uri" -> redirectedUri,
        "scope" -> "repo"
      )
    )
  }

  def completeOauth(sessionCode: String): Future[GithubUserToken] = {
    RequestBuilder.post[GithubUserToken](
      Uri("https://github.com/login/oauth/access_token").withQuery(
        Query(
          "client_id" -> getClientId,
          "client_secret" -> getClientSecret,
          "code" -> sessionCode
        )
      ),
      Seq(RawHeader("Accept", "application/json"))
    )
  }

  def getRepos(userToken: String): Future[List[Repo]] = {
    RequestBuilder.get[List[Repo]](
      uri = Uri(s"https://api.github.com/user/repos").withQuery(Query("type" -> "owner")),
      headers = Seq(RawHeader("Authorization", s"token $userToken"), RawHeader("Accept", "application/json"))
    )
  }

  def getCommits(repoFullName: String, userToken: String, since: Long, until: Long): Future[List[CommitInfo]] = {
    val userNameOnGithub = repoFullName.takeWhile(_ != '/')
    RequestBuilder.get[List[CommitInfo]](
      uri = Uri(s"https://api.github.com/repos/$repoFullName/commits").withQuery(
        Query(
          "author" -> userNameOnGithub,
          "since" -> toISO8601UTC(since),
          "until" -> toISO8601UTC(until)
        )
      ),
      headers = Seq(RawHeader("Authorization", s"token $userToken"), RawHeader("Accept", "application/json"))
    )
  }
}