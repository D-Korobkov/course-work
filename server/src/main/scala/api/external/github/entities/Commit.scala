package api.external.github.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class Commit(author: Author)

object Commit {
  implicit val jsonDecoder: Decoder[Commit] = deriveDecoder
  implicit val jsonEncoder: Encoder[Commit] = deriveEncoder
}
