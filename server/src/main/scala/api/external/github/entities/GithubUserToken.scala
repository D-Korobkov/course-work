package api.external.github.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class GithubUserToken(access_token: String, token_type: String, scope: String)

object GithubUserToken {
  implicit val jsonDecoder: Decoder[GithubUserToken] = deriveDecoder
  implicit val jsonEncoder: Encoder[GithubUserToken] = deriveEncoder
}
