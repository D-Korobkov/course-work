package api.external.github.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class CommitInfo(sha: String, commit: Commit)

object CommitInfo {
  implicit val jsonDecoder: Decoder[CommitInfo] = deriveDecoder
  implicit val jsonEncoder: Encoder[CommitInfo] = deriveEncoder
}