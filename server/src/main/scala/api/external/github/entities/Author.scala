package api.external.github.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class Author(date: String)

object Author {
  implicit val jsonDecoder: Decoder[Author] = deriveDecoder
  implicit val jsonEncoder: Encoder[Author] = deriveEncoder
}
