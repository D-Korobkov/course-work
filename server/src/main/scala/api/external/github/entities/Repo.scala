package api.external.github.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class Repo(full_name: String)

object Repo {
  implicit val jsonDecoder: Decoder[Repo] = deriveDecoder
  implicit val jsonEncoder: Encoder[Repo] = deriveEncoder
}
