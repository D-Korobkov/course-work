package api.external

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.{GET, POST}
import akka.http.scaladsl.model.{HttpHeader, HttpMethod, HttpRequest, Uri}
import akka.http.scaladsl.unmarshalling.{FromResponseUnmarshaller, Unmarshal}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}

object RequestBuilder extends LazyLogging {
  def simpleGet[T: FromResponseUnmarshaller](uri: Uri)
                                            (implicit executionContext: ExecutionContext, actors: ActorSystem): Future[T] = {
    get(uri, Nil)
  }

  def simplePost[T: FromResponseUnmarshaller](uri: Uri)
                                             (implicit executionContext: ExecutionContext, actors: ActorSystem): Future[T] = {
    post(uri, Nil)
  }

  def get[T: FromResponseUnmarshaller](uri: Uri, headers: Seq[HttpHeader])
                                      (implicit executionContext: ExecutionContext, actors: ActorSystem): Future[T] = {
    simpleRequest(GET, uri, headers)
  }

  def post[T: FromResponseUnmarshaller](uri: Uri, headers: Seq[HttpHeader])
                                       (implicit executionContext: ExecutionContext, actors: ActorSystem): Future[T] = {
    simpleRequest(POST, uri, headers)
  }

  private def simpleRequest[T: FromResponseUnmarshaller](method: HttpMethod, uri: Uri, headers: Seq[HttpHeader])
                                                        (implicit executionContext: ExecutionContext, actors: ActorSystem): Future[T] = {
    Http().singleRequest(HttpRequest(method, uri, headers)).flatMap(response => Unmarshal(response).to[T]).andThen(
      response => logger.info(uri + "\n" + response.toString)
    )
  }
}
