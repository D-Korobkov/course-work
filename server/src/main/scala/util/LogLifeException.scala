package util

sealed abstract class LogLifeException(message: String) extends Exception(message)

case class ForbiddenUserName(userName: String) extends LogLifeException(s"User name $userName is already in used.")

case class DatabaseException() extends LogLifeException(s"Something went wrong and your request was not handled. Try again later.")

case class InvalidCredentialsException(message: String = "Invalid login or password.") extends LogLifeException(message)

case class TooLongPassword(message: String = "Too long password.") extends LogLifeException(message)

case class MissingExternalApiCredentials(service: String) extends LogLifeException(s"Go to settings and allow access to $service")

case class ExternalApiCredentialsAlreadyExists(service: String) extends LogLifeException(s"You have already allowed access to $service")

case class UnauthorizedUserException() extends LogLifeException(s"Sign in or sign up please.")

case class InvalidCustomCategory(categoryName: String) extends LogLifeException(s"Category $categoryName does not exist")

case class DuplicateCustomCategory(categoryName: String) extends LogLifeException(s"You have already created category '$categoryName'")