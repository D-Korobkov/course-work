package util

import java.time.Instant
import java.time.format.DateTimeFormatter

object TimeUtil {
  private val secondsInDay = 86400

  def getCurrentTimeInSeconds: Long = Instant.now().getEpochSecond

  def getBeginningOfCurrentDayInSeconds(currentTime: Long): Long = {
    currentTime - currentTime % secondsInDay
  }

  def getBeginningOfNextDayInSeconds(currentTime: Long): Long = {
    val beginOfCurrentDay = getBeginningOfCurrentDayInSeconds(currentTime)
    beginOfCurrentDay + secondsInDay
  }

  def toISO8601UTC(time: Long): String = {
    DateTimeFormatter.ISO_INSTANT.format(Instant.ofEpochSecond(time))
  }

  def fromISO8601UTC(date: String): Long = {
    Instant.from(DateTimeFormatter.ISO_INSTANT.parse(date)).getEpochSecond
  }
}

