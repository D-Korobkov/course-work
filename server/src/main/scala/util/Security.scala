package util

import java.time.Instant

import com.github.t3hnar.bcrypt._
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}

import scala.util.Try

object Security {
  def claim: JwtClaim = JwtClaim(
    expiration = Some(Instant.now.plusSeconds(3600).getEpochSecond),
    issuedAt = Some(Instant.now.getEpochSecond)
  )

  def generateToken: String = JwtCirce.encode(claim, "secret", JwtAlgorithm.HS512)

  def hashPassword: String => Try[String] = _.bcryptSafeBounded(10)

  def verifyPassword(text: String, hash: String): Try[Boolean] = text.isBcryptedSafeBounded(hash)
}