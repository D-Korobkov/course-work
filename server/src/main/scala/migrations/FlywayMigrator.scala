package migrations

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.LazyLogging
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.output.MigrateResult

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class FlywayMigrator(configPath: String, configName: String)(implicit ex: ExecutionContext) extends LazyLogging {
  private val dbConfig: Config = ConfigFactory.parseFile(new File(configPath)).getConfig(configName)

  private val flyway: Flyway = {
    Flyway
      .configure()
      .dataSource(dbConfig.getString("url"), dbConfig.getString("username"), dbConfig.getString("password"))
      .locations("filesystem:server/src/main/scala/migrations/sql")
      .baselineOnMigrate(true)
      .load()
  }

  def connectAndMigrate(): Future[MigrateResult] = {
    Future(flyway.migrate()).andThen {
      case Success(_) => logger.info("Database migration & connection test complete")
      case Failure(exception) => logger.info("Can not init database: " + exception)
    }
  }
}
