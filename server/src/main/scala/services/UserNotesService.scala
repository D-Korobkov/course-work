package services

import java.io.File
import java.util.concurrent.{ScheduledExecutorService, TimeUnit}

import api.internal.entities.{UserCustomNote, UserNotes, UserStandardNote}
import com.github.tototoshi.csv.CSVWriter
import db.entities.notes.{CodeforcesNote, CustomNote, GithubNote, StandardNote, StravaNote}
import db.repositories.UserQueryRepository
import db.repositories.notes._
import io.circe.Json
import io.circe.generic.auto.exportEncoder
import io.circe.syntax.EncoderOps
import slick.jdbc.SQLiteProfile.api._
import util.{InvalidCustomCategory, TimeUtil}

import scala.concurrent.{ExecutionContext, Future, Promise}

class UserNotesService(implicit db: Database, ex: ExecutionContext, scheduler: ScheduledExecutorService) {
  type AllNotes = (Seq[StandardNote], Seq[CustomNote], Seq[CodeforcesNote], Seq[GithubNote], Seq[StravaNote])

  def addNewStandardNote(userName: String, note: UserStandardNote): Future[Boolean] = {
    val todayEnd = TimeUtil.getBeginningOfNextDayInSeconds(TimeUtil.getCurrentTimeInSeconds)
    val newNote = StandardNote(
      todayEnd, userName, note.mood, note.productivity, note.awakenTime, note.asleepTime, note.revenues, note.expenses
    )

    db.run {
      StandardNotesRepository.getUserNotesInSpecifiedDay(userName, todayEnd).flatMap { res =>
        if (res.nonEmpty)
          StandardNotesRepository.replaceOldNote(userName, newNote)
        else
          StandardNotesRepository.addNote(newNote)
      }
    }.map(_ == 1)
  }

  def addNewCustomNote(userName: String, note: UserCustomNote): Future[Boolean] = {
    val checkingCategoryName = db.run(UserQueryRepository.getUserCustomCategories(userName)).flatMap {
      case categories if !categories.contains(note.categoryName) => Future.failed(InvalidCustomCategory(note.categoryName))
      case _ => Future.successful(true)
    }
    val todayEnd = TimeUtil.getBeginningOfNextDayInSeconds(TimeUtil.getCurrentTimeInSeconds)
    val newNote = CustomNote(todayEnd, userName, note.categoryName, note.text)

    checkingCategoryName.flatMap { _ =>
      db.run {
        CustomNotesRepository.findSpecifiedCategoryNoteByDay(userName, note.categoryName, todayEnd).flatMap { res =>
          if (res.isDefined)
            CustomNotesRepository.replaceOldNote(userName, newNote)
          else
            CustomNotesRepository.addNote(newNote)
        }
      }.map(_ == 1)
    }
  }

  def getSpecifiedDayNoteAsJson(userName: String, day: Long): Future[Json] = {
    getSpecifiedDayNote(userName, day).map(_.asJson)
  }

  def getSpecifiedDayStandardNoteAsJson(userName: String, day: Long): Future[Json] = {
    db.run(StandardNotesRepository.getUserNotesInSpecifiedDay(userName, day)).map(_.headOption.asJson)
  }

  def getSpecifiedDayCustomNoteAsJson(userName: String, categoryName: String, day: Long): Future[Json] = {
    db.run(CustomNotesRepository.findSpecifiedCategoryNoteByDay(userName, categoryName, day)).map(_.asJson)
  }

  def getAllNotesAsJson(userName: String): Future[Json] = {
    getAllUserNotes(userName).map(t => UserNotes(t._1, t._2, t._3, t._4, t._5)).map(_.asJson)
  }

  def createFileWithUserNotes(userName: String): Future[String] = {
    val fileName = s"private/$userName.csv"
    val file = new File(fileName)
    if (file.exists()) {
      Future.successful(fileName)
    } else {
      deleteFileOnTimeout(file)
      val writer: CSVWriter = CSVWriter.open(file)
      putDiaryInfoIntoFile(userName, fileName, writer)
    }
  }

  private def deleteFileOnTimeout(file: File, timeout: Long = 1): Future[Unit] = {
    val promise = Promise[Unit]()
    scheduler.schedule(() => promise.success(file.delete()), timeout, TimeUnit.MINUTES)
    promise.future
  }

  private def putDiaryInfoIntoFile(userName: String, fileName: String, writer: CSVWriter): Future[String] = {
    getAllUserNotes(userName)
      .map(t => writer.writeAll(Seq(t._1, t._2, t._3, t._4, t._5))).map(_ => fileName).andThen(_ => writer.close())
  }

  private def getAllUserNotes(userName: String): Future[AllNotes] = {
    val standardNotes = db.run(StandardNotesRepository.getSortedAllUsersNotes(userName))
    val customNotes = db.run(CustomNotesRepository.getSortedAllUsersNotes(userName))
    val cfNotes = db.run(CodeforcesNotesRepository.getSortedAllUsersNotes(userName))
    val ghNotes = db.run(GithubNotesRepository.getSortedAllUsersNotes(userName))
    val stravaNotes = db.run(StravaNotesRepository.getSortedAllUsersNotes(userName))
    standardNotes.zip(customNotes).zip(cfNotes).zip(ghNotes).zip(stravaNotes).map {
      case ((((note1, note2), note3), note4), note5) => (note1, note2, note3, note4, note5)
    }
  }

  private def getSpecifiedDayNote(userName: String, day: Long): Future[UserNotes] = {
    val cfNotes = db.run(CodeforcesNotesRepository.getUserNotesInSpecifiedDay(userName, day))
    val ghNotes = db.run(GithubNotesRepository.getUserNotesInSpecifiedDay(userName, day))
    val stravaNotes = db.run(StravaNotesRepository.getUserNotesInSpecifiedDay(userName, day))
    val standardNotes = db.run(StandardNotesRepository.getUserNotesInSpecifiedDay(userName, day))
    val customNotes = db.run(CustomNotesRepository.getUserNotesInSpecifiedDay(userName, day))

    standardNotes.zip(customNotes).zip(cfNotes).zip(ghNotes).zip(stravaNotes).map {
      case ((((note1, note2), note3), note4), note5) => UserNotes(note1, note2, note3, note4, note5)
    }
  }
}
