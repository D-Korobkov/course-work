package services

import akka.http.scaladsl.model.Uri
import api.external.github.GithubApi
import api.external.github.entities.{CommitInfo, Repo}
import db.entities.notes.GithubNote
import db.repositories.UserQueryRepository
import db.repositories.UserQueryRepository.getUserGithubTokenId
import db.repositories.notes.GithubNotesRepository
import db.repositories.tokens.external.GithubQueryRepository._
import io.circe.Json
import io.circe.generic.auto.exportEncoder
import io.circe.syntax.EncoderOps
import slick.jdbc.SQLiteProfile.api._
import util.TimeUtil._
import util.{ExternalApiCredentialsAlreadyExists, MissingExternalApiCredentials}

import scala.concurrent.{ExecutionContext, Future}

class GithubUserService(implicit db: Database, executionContext: ExecutionContext, githubApi: GithubApi) {
  def getRedirectLink(userName: String): Future[Uri] = {
    db.run(getUserGithubTokenId(userName)).flatMap { id =>
      if (id.isDefined)
        Future.failed(ExternalApiCredentialsAlreadyExists("Github"))
      else
        githubApi.getLinkToGithubAuthPage(s"http://localhost:8080/github/oauth/$userName")
    }
  }

  def completeOauth(userName: String, sessionCode: String): Future[Boolean] = {
    githubApi.completeOauth(sessionCode).flatMap { token =>
      db.run {
        addGithubUserToken(token).andThen(
          getIdByAccessToken(token.access_token).flatMap(tokenId =>
            UserQueryRepository.updateGithubTokenId(userName, tokenId).map(_ == 1)
          )
        )
      }
    }
  }

  def fillGithubNotes(userName: String): Future[Int] = {
    val timeLimit = getBeginningOfCurrentDayInSeconds(getCurrentTimeInSeconds)
    getGithubTokenByUserName(userName).map(_.get).flatMap { token =>
      createNewGithubNotes(userName, token, timeLimit).flatMap { notes =>
        db.run {
          GithubNotesRepository.addNotes(notes) >> updateLastUpdatedByToken(token, timeLimit)
        }
      }
    }
  }

  def getAllNotesAsJson(userName: String): Future[Json] = {
    getAllNotes(userName).map(_.asJson)
  }

  def getAllNotes(userName: String): Future[Seq[GithubNote]] = {
    db.run {
      GithubNotesRepository.getAllUsersNotes(userName)
    }
  }

  private def getGithubTokenByUserName(userName: String): Future[Option[String]] = {
    db.run(getUserGithubTokenId(userName)).flatMap { maybeTokenId: Option[Int] =>
      if (maybeTokenId.isDefined)
        db.run(getAccessTokenById(maybeTokenId.get))
      else
        Future.failed(MissingExternalApiCredentials("Github"))
    }
  }

  private def createNewGithubNotes(userName: String, token: String, timeLimit: Long): Future[List[GithubNote]] = {
    val userRepositories = githubApi.getRepos(token)

    getLastDayWhenGithubNotesWereUpdated(token).flatMap { fromTime =>
      userRepositories.flatMap { repositories =>
        Future.traverse(repositories)(getCommitsFromRepository(userName, token, fromTime, timeLimit))
      }.map(_.flatten)
    }
  }

  private def getCommitsFromRepository(userName: String, token: String, since: Long, until: Long)(repo: Repo): Future[List[GithubNote]] = {
    githubApi.getCommits(repo.full_name, token, since, until).map { listOfCommits =>
      val numberOfCommitsPerDay = getNumberOfCommitsPerDay(listOfCommits)
      LazyList.iterate(getBeginningOfNextDayInSeconds(since))(getBeginningOfNextDayInSeconds)
        .takeWhile(_ <= until)
        .map(day => GithubNote(day, userName, repo.full_name.dropWhile(_ != '/').tail, numberOfCommitsPerDay.getOrElse(day, 0)))
        .toList
    }
  }

  private def getNumberOfCommitsPerDay(listOfCommits: List[CommitInfo]): Map[Long, Int] = {
    listOfCommits
      .groupBy(commitInfo => getBeginningOfNextDayInSeconds(fromISO8601UTC(commitInfo.commit.author.date)))
      .transform { case (_, commits) => commits.length }
  }

  private def getLastDayWhenGithubNotesWereUpdated(token: String): Future[Long] = {
    db.run {
      getLastUpdatedByToken(token)
    }.flatMap {
      case None => Future.failed(MissingExternalApiCredentials("Github"))
      case Some(time) => Future.successful(time)
    }
  }
}
