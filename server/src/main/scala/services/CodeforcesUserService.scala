package services

import api.external.codeforces.CodeforcesApi
import api.external.codeforces.entities.{RatingChange, Submission, User}
import db.entities.notes.CodeforcesNote
import db.repositories.UserQueryRepository._
import db.repositories.notes.CodeforcesNotesRepository
import db.repositories.tokens.external.CodeforcesQueryRepository._
import io.circe.Json
import io.circe.generic.auto.exportEncoder
import io.circe.syntax.EncoderOps
import slick.jdbc.SQLiteProfile.api._
import util.TimeUtil._
import util.{ExternalApiCredentialsAlreadyExists, InvalidCredentialsException, MissingExternalApiCredentials}

import scala.concurrent.{ExecutionContext, Future}

class CodeforcesUserService(implicit db: Database, codeforcesApi: CodeforcesApi, executionContext: ExecutionContext) {
  def getUserInfoByNickname(userName: String, nickname: String): Future[User] = {
    db.run(getUserCodeforcesTokenId(userName)).flatMap { id =>
      if (id.isDefined)
        Future.failed(ExternalApiCredentialsAlreadyExists("Codeforces"))
      else
        codeforcesApi
          .getUsersInfo(List(nickname))
          .transform(identity, _ => InvalidCredentialsException("Incorrect codeforces handle."))
          .map(_.result.head)
    }
  }

  def getAllNotesAsJson(userName: String): Future[Json] = {
    getAllNotes(userName).map(_.asJson)
  }

  def getAllNotes(userName: String): Future[Seq[CodeforcesNote]] = {
    db.run {
      CodeforcesNotesRepository.getAllUsersNotes(userName)
    }
  }

  def updateCodeforcesHandle(userName: String, userCollection: User): Future[Boolean] = {
    db.run {
      addCodeforcesHandle(userCollection.handle) >>
        getIdByAccessToken(userCollection.handle).flatMap(tokenId => updateCodeforcesTokenId(userName, tokenId).map(_ == 1))
    }
  }

  def fillCodeforcesNotes(userName: String): Future[Int] = {
    val timeLimit = getBeginningOfCurrentDayInSeconds(getCurrentTimeInSeconds)
    getCodeforcesHandleByUserName(userName).map(_.get).flatMap { handle =>
      createNewCodeforcesNotes(userName, handle, timeLimit).flatMap { notes =>
        db.run {
          CodeforcesNotesRepository.addNotes(notes) >> updateLastUpdatedByToken(handle, timeLimit)
        }
      }
    }
  }

  private def createNewCodeforcesNotes(userName: String, handle: String, timeLimit: Long): Future[List[CodeforcesNote]] = {
    val lastDayUpdated: Future[Long] = getLastDayWhenCodeforcesNotesWereUpdated(handle)

    getRatingChanges(handle, lastDayUpdated, timeLimit)
      .zip(getNumberOfSolvedTasks(handle, lastDayUpdated, timeLimit))
      .zip(lastDayUpdated)
      .map { tuple =>
        LazyList.iterate(getBeginningOfNextDayInSeconds(tuple._2))(getBeginningOfNextDayInSeconds)
          .takeWhile(_ <= timeLimit)
          .map(day => CodeforcesNote(day, userName, tuple._1._1.getOrElse(day, 0), tuple._1._2.getOrElse(day, 0)))
          .toList
      }
  }

  private def getLastDayWhenCodeforcesNotesWereUpdated(handle: String): Future[Long] = {
    db.run {
      getLastUpdatedByToken(handle)
    }.flatMap {
      case None => Future.failed(MissingExternalApiCredentials("Codeforces"))
      case Some(time) => Future.successful(time)
    }
  }

  private def getCodeforcesHandleByUserName(userName: String): Future[Option[String]] = {
    db.run(getUserCodeforcesTokenId(userName)).flatMap { maybeTokenId: Option[Int] =>
      if (maybeTokenId.isDefined)
        db.run(getAccessTokenById(maybeTokenId.get))
      else
        Future.failed(MissingExternalApiCredentials("Codeforces"))
    }
  }

  private def getNumberOfSolvedTasks(handle: String, lastDayUpdated: Future[Long], timeLimit: Long): Future[Map[Long, Int]] = {
    codeforcesApi.getSubmissionsInfo(handle).zip(lastDayUpdated).map {
      case (tasksList, lastUpdated) =>
        getNumberOfSolvedTasksPerDay {
          tasksList.result.filter { task =>
            task.verdict.contains("OK") && task.creationTimeSeconds >= lastUpdated && task.creationTimeSeconds < timeLimit
          }
        }
    }
  }

  private def getNumberOfSolvedTasksPerDay(solvedTasks: List[Submission]): Map[Long, Int] = {
    solvedTasks.groupBy { task =>
      getBeginningOfNextDayInSeconds(task.creationTimeSeconds)
    }.transform {
      case (_, tasks) => tasks.length
    }
  }

  private def getRatingChanges(handle: String, lastDayUpdated: Future[Long], timeLimit: Long): Future[Map[Long, Int]] = {
    codeforcesApi.getRatingChangeInfo(handle).zip(lastDayUpdated).map {
      case (ratingChanges, lastUpdated) =>
        getRatingChangePerDay {
          ratingChanges.result.filter { ratingChange =>
            ratingChange.ratingUpdateTimeSeconds >= lastUpdated && ratingChange.ratingUpdateTimeSeconds < timeLimit
          }
        }
    }
  }

  private def getRatingChangePerDay(ratingChanges: List[RatingChange]): Map[Long, Int] = {
    ratingChanges.groupBy { ratingChange =>
      getBeginningOfNextDayInSeconds(ratingChange.ratingUpdateTimeSeconds)
    }.transform {
      case (_, list) => list.maxBy(_.ratingUpdateTimeSeconds).newRating - list.minBy(_.ratingUpdateTimeSeconds).oldRating
    }
  }
}
