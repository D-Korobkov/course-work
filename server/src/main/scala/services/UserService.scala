package services

import api.internal.entities._
import db.entities.User
import db.entities.tokens.LogLifeToken
import db.repositories.UserQueryRepository
import db.repositories.tokens.LogLifeQueryRepository
import io.circe.Json
import io.circe.syntax.EncoderOps
import slick.jdbc.SQLiteProfile.api._
import util._

import scala.concurrent.{ExecutionContext, Future}

class UserService(implicit db: Database, executionContext: ExecutionContext) {
  def registerUser(credentials: LoginRequest): Future[String] = {
    val hashingPwd = Future(Security.hashPassword(credentials.password).toOption)
    db.run {
      UserQueryRepository.findUserByUserName(credentials.username)
    }.zip(hashingPwd).flatMap {
      case (Some(_), _) => Future.failed(ForbiddenUserName(credentials.username))
      case (_, None) => Future.failed(TooLongPassword())
      case (None, Some(pwd)) => addNewUserToDatabase(credentials.copy(password = pwd))
    }
  }

  def addNewUserToDatabase(credentials: LoginRequest): Future[String] = {
    val user = User(credentials.username, credentials.password)
    db.run {
      UserQueryRepository.addNewUser(user)
    }.map(_ => user.userName)
  }

  def verifyUserSecrets(request: LoginRequest): Future[Boolean] = {
    db.run {
      UserQueryRepository.getUserPassword(request.username)
    }.map {
      _.flatMap(hash => Security.verifyPassword(request.password, hash).toOption).getOrElse(false)
    }
  }

  def createAccessToken(userName: String): Future[String] = {
    val token = Security.generateToken
    db.run {
      LogLifeQueryRepository.addToken(LogLifeToken(token, userName, Security.claim.expiration.get))
    }.flatMap(added => if (added == 1) Future.successful(token) else Future.failed(DatabaseException()))
  }

  def checkAuthentication(token: String): Future[Option[String]] = {
    db.run {
      LogLifeQueryRepository.getToken(token)
    }.flatMap {
      case None => Future.successful(None)
      case Some(token) if token.expiresAt < java.time.Instant.now.getEpochSecond =>
        db.run(LogLifeQueryRepository.deleteToken(token.token)).map(_ => None)
      case Some(token) => Future.successful(Some(token.userName))
    }
  }

  def addCustomCategory(userName: String, categoryName: String): Future[Int] = {
    db.run(UserQueryRepository.getUserCustomCategories(userName)).flatMap { categories =>
      if (categories.contains(categoryName)) {
        Future.failed(DuplicateCustomCategory(categoryName))
      } else {
        db.run(UserQueryRepository.updateCustomCategories(userName, categoryName +: categories))
      }
    }
  }

  def getAllUsersCategories(userName: String): Future[Json] = {
    db.run(UserQueryRepository.getUserCategories(userName)).map {
      case (customCategories, codeforcesAvailable, githubAvailable, stravaAvailable) =>
        UserAvailableCategories(
          custom = customCategories,
          codeforces = codeforcesAvailable,
          github = githubAvailable,
          strava = stravaAvailable
        ).asJson
    }
  }
}
