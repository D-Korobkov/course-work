package services

import akka.http.scaladsl.model.Uri
import api.external.strava.StravaApi
import api.external.strava.entities.{StravaUserToken, SummaryActivity}
import db.entities.notes.StravaNote
import db.repositories.UserQueryRepository
import db.repositories.UserQueryRepository.getUserStravaTokenId
import db.repositories.notes.StravaNotesRepository
import db.repositories.tokens.external.StravaQueryRepository._
import io.circe.Json
import io.circe.generic.auto.exportEncoder
import io.circe.syntax.EncoderOps
import slick.jdbc.SQLiteProfile.api._
import util.TimeUtil._
import util.{ExternalApiCredentialsAlreadyExists, MissingExternalApiCredentials}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

class StravaUserService(implicit db: Database, executionContext: ExecutionContext, stravaApi: StravaApi) {
  def getRedirectLink(userName: String): Future[Uri] = {
    db.run(getUserStravaTokenId(userName)).flatMap { id =>
      if (id.isDefined)
        Future.failed(ExternalApiCredentialsAlreadyExists("Strava"))
      else
        stravaApi.getLinkToScalaAuthPage(s"http://localhost:8080/strava/oauth/$userName")
    }
  }

  def completeOauth(userName: String, sessionCode: String): Future[Boolean] = {
    stravaApi.completeScalaOauth(sessionCode).flatMap { token =>
      db.run {
        addStravaUserToken(token).andThen(
          getIdByAccessToken(token.access_token).flatMap(tokenId =>
            UserQueryRepository.updateStravaTokenId(userName, tokenId).map(_ == 1)
          )
        )
      }
    }
  }

  def getAllNotesAsJson(userName: String): Future[Json] = {
    getAllNotes(userName).map(_.asJson)
  }

  def getAllNotes(userName: String): Future[Seq[StravaNote]] = {
    db.run {
      StravaNotesRepository.getAllUsersNotes(userName)
    }
  }

  def fillStravaNotes(userName: String): Future[Int] = {
    val now = getCurrentTimeInSeconds
    val timeLimit = getBeginningOfCurrentDayInSeconds(now)

    getStravaAccessTokenByUserName(userName, now).flatMap { token =>
      createNewStravaNotes(userName, token, timeLimit).flatMap { notes =>
        db.run {
          StravaNotesRepository.addNotes(notes) >> updateLastUpdatedByToken(token, timeLimit)
        }
      }
    }
  }

  private def getStravaAccessTokenByUserName(userName: String, currentTime: Long): Future[String] = {
    db.run(getUserStravaTokenId(userName)).flatMap {
      case Some(id) =>
        db.run(getTokenExpiresAtTime(id)).flatMap { expiresAtTime =>
          if (expiresAtTime.get <= currentTime)
            getFreshAccessTokenById(id).map(_.access_token)
          else
            db.run(getAccessTokenById(id)).map(_.get)
        }
      case _ => Future.failed(MissingExternalApiCredentials("Strava"))
    }
  }

  private def getFreshAccessTokenById(tokenId: Int): Future[StravaUserToken] = {
    db.run {
      getRefreshTokenById(tokenId)
    }.flatMap { refreshToken =>
      stravaApi.refreshAccessToken(refreshToken.get).andThen {
        case Success(token) => db.run(updateToken(tokenId, token))
      }
    }
  }

  private def createNewStravaNotes(userName: String, token: String, timeLimit: Long): Future[List[StravaNote]] = {
    getLastDayWhenStravaNotesWereUpdated(token).flatMap { fromTime =>
      getActivitiesPerDay(token, fromTime, timeLimit).map { activitiesPerDay: Map[Long, Seq[SummaryActivity]] =>
        LazyList.iterate(getBeginningOfNextDayInSeconds(fromTime))(getBeginningOfNextDayInSeconds)
          .takeWhile(_ <= timeLimit)
          .flatMap { day =>
            val activities = activitiesPerDay.getOrElse(day, Seq.empty[SummaryActivity])
            if (activities.isEmpty) {
              Seq(StravaNote(day, userName, "Nothing", 0))
            } else {
              activities.map(activity => StravaNote(day, userName, activity.`type`, activity.distance))
            }
          }
          .toList
      }
    }
  }

  private def getActivitiesPerDay(token: String, since: Long, until: Long, page: Int = 1): Future[Map[Long, Seq[SummaryActivity]]] = {
    stravaApi.getUserActivities(token, since, until, page).flatMap { activities: Seq[SummaryActivity] =>
      if (activities.nonEmpty) {
        val otherActivitiesPerDay = getActivitiesPerDay(token, since, until, page + 1)
        val activitiesPerDay = activities.groupBy(activity => getBeginningOfNextDayInSeconds(fromISO8601UTC(activity.start_date)))

        otherActivitiesPerDay.map(res => List(activitiesPerDay, res).flatten.groupMapReduce(_._1)(_._2)(_ ++ _)) // merging
      } else {
        Future.successful(Map.empty[Long, Seq[SummaryActivity]])
      }
    }
  }

  private def getLastDayWhenStravaNotesWereUpdated(token: String): Future[Long] = {
    db.run {
      getLastUpdatedByToken(token)
    }.flatMap {
      case None => Future.failed(MissingExternalApiCredentials("Strava"))
      case Some(time) => Future.successful(time)
    }
  }
}
