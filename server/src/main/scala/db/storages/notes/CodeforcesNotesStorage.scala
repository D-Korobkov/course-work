package db.storages.notes

import db.entities.notes.CodeforcesNote
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class CodeforcesNotesStorage(tag: Tag) extends NotesStorage[CodeforcesNote](tag, "CODEFORCES_NOTES") {
  def ratingChange: Rep[Int] = column("RATING_CHANGE")

  def solvedTasks: Rep[Int] = column("SOLVED_TASKS")

  override def * : ProvenShape[CodeforcesNote] = {
    (day, ownerName, ratingChange, solvedTasks).mapTo[CodeforcesNote]
  }
}
