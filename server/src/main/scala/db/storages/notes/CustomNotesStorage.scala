package db.storages.notes

import db.entities.notes.CustomNote
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class CustomNotesStorage(tag: Tag) extends NotesStorage[CustomNote](tag, "CUSTOM_NOTES") {
  def categoryName: Rep[String] = column("CATEGORY_NAME")

  def text: Rep[Option[String]] = column("TEXT")

  def * : ProvenShape[CustomNote] = (day, ownerName, categoryName, text).mapTo[CustomNote]
}
