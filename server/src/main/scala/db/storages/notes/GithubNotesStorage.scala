package db.storages.notes

import db.entities.notes.GithubNote
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class GithubNotesStorage(tag: Tag) extends NotesStorage[GithubNote](tag, "GITHUB_NOTES") {
  def repositoryName: Rep[String] = column("REPO")

  def numberOfCommits: Rep[Int] = column("COMMITS")

  override def * : ProvenShape[GithubNote] = (day, ownerName, repositoryName, numberOfCommits).mapTo[GithubNote]
}
