package db.storages.notes

import db.entities.notes.StandardNote
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class StandardNotesStorage(tag: Tag) extends NotesStorage[StandardNote](tag, "STANDARD_NOTES") {
  def mood: Rep[Option[Int]] = column("MOOD")

  def productivity: Rep[Option[Int]] = column("PRODUCTIVITY")

  def awakenTime: Rep[Option[Long]] = column("AWAKEN_TIME") // UNIX-time

  def asleepTime: Rep[Option[Long]] = column("ASLEEP_TIME") // UNIX-time

  def revenues: Rep[Option[Int]] = column("REVENUES")

  def expenses: Rep[Option[Int]] = column("EXPENSES")

  def * : ProvenShape[StandardNote] = {
    (day, ownerName, mood, productivity, awakenTime, asleepTime, revenues, expenses).mapTo[StandardNote]
  }
}
