package db.storages.notes

import slick.jdbc.H2Profile.api._

abstract class NotesStorage[T](tag: Tag, name: String) extends Table[T](tag, name) {
  def day: Rep[Long] = column("DAY")

  def ownerName: Rep[String] = column("Author")
}
