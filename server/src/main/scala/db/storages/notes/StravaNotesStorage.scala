package db.storages.notes

import db.entities.notes.StravaNote
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class StravaNotesStorage(tag: Tag) extends NotesStorage[StravaNote](tag, "STRAVA_NOTES") {
  def activityType: Rep[String] = column("ACTIVITY")

  def distance: Rep[Double] = column("DISTANCE")

  override def * : ProvenShape[StravaNote] = (day, ownerName, activityType, distance).mapTo[StravaNote]
}
