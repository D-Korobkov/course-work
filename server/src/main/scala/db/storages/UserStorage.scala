package db.storages

import db.entities.User
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class UserStorage(tag: Tag) extends Table[User](tag, "USERS") {
  def userName: Rep[String] = column("USER_NAME", O.Unique)

  def password: Rep[String] = column("PASSWORD")

  def stravaTokenId: Rep[Option[Int]] = column("STRAVA_TOKEN_ID")

  def githubTokenId: Rep[Option[Int]] = column("GITHUB_TOKEN_ID")

  def codeforcesTokenId: Rep[Option[Int]] = column("CODEFORCES_TOKEN_ID")

  def customCategories: Rep[String] = column("CUSTOM_CATEGORIES") // "cat1;cat2;cat3"

  override def * : ProvenShape[User] = {
    (userName, password, stravaTokenId, githubTokenId, codeforcesTokenId, customCategories).mapTo[User]
  }
}