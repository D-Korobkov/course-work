package db.storages.tokens

import db.entities.tokens.LogLifeToken
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class LogLifeTokenStorage(tag: Tag) extends Table[LogLifeToken](tag, "SESSIONS") {
  def token: Rep[String] = column("TOKEN", O.Unique)

  def userName: Rep[String] = column("USER_NAME")

  def expiresAt: Rep[Long] = column("EXPIRES_AT")

  override def * : ProvenShape[LogLifeToken] = (token, userName, expiresAt).mapTo[LogLifeToken]
}
