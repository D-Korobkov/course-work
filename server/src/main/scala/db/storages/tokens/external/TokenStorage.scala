package db.storages.tokens.external

import slick.jdbc.H2Profile.api._

abstract class TokenStorage[T](tag: Tag, name: String) extends Table[T](tag, name) {
  def tokenId: Rep[Int] = column("TOKEN_ID", O.PrimaryKey, O.AutoInc)

  def accessToken: Rep[String] = column("ACCESS_TOKEN")

  def lastUpdated: Rep[Long] = column("LAST_DAY_UPDATED")
}
