package db.storages.tokens.external

import db.entities.tokens.StravaToken
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class StravaTokenStorage(tag: Tag) extends TokenStorage[StravaToken](tag, "STRAVA_TOKENS") {
  def tokenType: Rep[String] = column("TOKEN_TYPE")

  def refreshToken: Rep[String] = column("REFRESH_TOKEN")

  def expiresIn: Rep[Long] = column("EXPIRES_IN")

  def expiresAt: Rep[Long] = column("EXPIRES_AT")

  override def * : ProvenShape[StravaToken] = {
    (tokenId.?, tokenType, accessToken, refreshToken, expiresIn, expiresAt, lastUpdated).mapTo[StravaToken]
  }
}
