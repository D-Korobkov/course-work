package db.storages.tokens.external

import db.entities.tokens.GithubToken
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class GithubTokenStorage(tag: Tag) extends TokenStorage[GithubToken](tag, "GITHUB_TOKENS") {
  def tokenType: Rep[String] = column("TOKEN_TYPE")

  def scope: Rep[String] = column("SCOPE")

  override def * : ProvenShape[GithubToken] = (tokenId.?, tokenType, accessToken, scope, lastUpdated).mapTo[GithubToken]
}