package db.storages.tokens.external

import db.entities.tokens.CodeforcesToken
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

class CodeforcesTokenStorage(tag: Tag) extends TokenStorage[CodeforcesToken](tag, "CODEFORCES_TOKENS") {
  override def * : ProvenShape[CodeforcesToken] = (tokenId.?, accessToken, lastUpdated).mapTo[CodeforcesToken]
}
