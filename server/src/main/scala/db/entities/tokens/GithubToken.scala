package db.entities.tokens

import util.TimeUtil._

case class GithubToken(tokenId: Option[Int],
                       tokenType: String,
                       accessToken: String,
                       scope: String,
                       lastUpdated: Long = 1609459200)//getBeginningOfCurrentDayInSeconds(getCurrentTimeInSeconds))
