package db.entities.tokens

case class StravaToken(tokenId: Option[Int],
                       tokenType: String,
                       accessToken: String,
                       refreshToken: String,
                       expiresIn: Long,
                       expiresAt: Long,
                       lastUpdated: Long = 1609459200)//getBeginningOfCurrentDayInSeconds(getCurrentTimeInSeconds))
