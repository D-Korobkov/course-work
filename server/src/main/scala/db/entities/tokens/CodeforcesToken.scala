package db.entities.tokens

import util.TimeUtil._

case class CodeforcesToken(tokenId: Option[Int],
                           handle: String,
                           lastUpdated: Long = 1609459200)//getBeginningOfCurrentDayInSeconds(getCurrentTimeInSeconds))
