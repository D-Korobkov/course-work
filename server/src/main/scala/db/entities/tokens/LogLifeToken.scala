package db.entities.tokens

case class LogLifeToken(token: String, userName: String, expiresAt: Long)
