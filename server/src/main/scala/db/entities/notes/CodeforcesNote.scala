package db.entities.notes

case class CodeforcesNote(day: Long,
                          ownerName: String,
                          ratingChange: Int,
                          solvedTasks: Int)
