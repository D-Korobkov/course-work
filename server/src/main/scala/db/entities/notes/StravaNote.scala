package db.entities.notes

case class StravaNote(day: Long,
                      ownerName: String,
                      activityType: String,
                      distance: Double)
