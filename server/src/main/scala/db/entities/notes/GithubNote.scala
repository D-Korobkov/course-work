package db.entities.notes

case class GithubNote(day: Long,
                      ownerName: String,
                      repositoryName: String,
                      numberOfCommits: Int)
