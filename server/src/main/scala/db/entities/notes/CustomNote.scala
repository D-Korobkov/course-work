package db.entities.notes

case class CustomNote(day: Long,
                      ownerName: String,
                      categoryName: String,
                      text: Option[String])
