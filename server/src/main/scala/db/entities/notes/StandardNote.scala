package db.entities.notes

case class StandardNote(day: Long,
                        ownerName: String,
                        mood: Option[Int],
                        productivity: Option[Int],
                        awakenTime: Option[Long],
                        asleepTime: Option[Long],
                        revenues: Option[Int],
                        expenses: Option[Int])
