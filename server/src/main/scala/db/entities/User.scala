package db.entities

case class User(userName: String,
                password: String,
                stravaTokenId: Option[Int] = None,
                githubTokenId: Option[Int] = None,
                codeforcesNickname: Option[Int] = None,
                customCategories: String = "")