package db.repositories.notes

import db.entities.notes._
import db.repositories.DIO
import db.storages.notes._
import slick.dbio.Effect
import slick.jdbc.H2Profile.api._
import slick.lifted.Tag

// standard
object StandardNotesRepository extends NotesRepository[StandardNote, StandardNotesStorage]((tag: Tag) => new StandardNotesStorage(tag)) {
  def replaceOldNote(userName: String, newNote: StandardNote): DIO[Int, Effect.Write] = {
    AllNotes.filter(rep => rep.ownerName === userName && rep.day === newNote.day).update(newNote)
  }
}

// custom
object CustomNotesRepository extends NotesRepository[CustomNote, CustomNotesStorage]((tag: Tag) => new CustomNotesStorage(tag)) {
  def getUserNotesOfSpecifiedCategory(userName: String, categoryName: String): DIO[Seq[CustomNote], Effect.Read] = {
    AllNotes.filter(rep => rep.ownerName === userName && rep.categoryName === categoryName).result
  }

  def replaceOldNote(userName: String, newNote: CustomNote): DIO[Int, Effect.Write] = {
    AllNotes.filter(rep => rep.ownerName === userName && rep.categoryName === newNote.categoryName && rep.day === newNote.day).update(newNote)
  }

  def findSpecifiedCategoryNoteByDay(userName: String, categoryName: String, day: Long): DIO[Option[CustomNote], Effect.Read] = {
    AllNotes.filter(rep => rep.ownerName === userName && rep.categoryName === categoryName && rep.day === day).result.headOption
  }
}

// codeforces
object CodeforcesNotesRepository extends NotesRepository[CodeforcesNote, CodeforcesNotesStorage]((tag: Tag) => new CodeforcesNotesStorage(tag))

// github
object GithubNotesRepository extends NotesRepository[GithubNote, GithubNotesStorage]((tag: Tag) => new GithubNotesStorage(tag))

// strava
object StravaNotesRepository extends NotesRepository[StravaNote, StravaNotesStorage]((tag: Tag) => new StravaNotesStorage(tag))