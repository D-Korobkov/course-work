package db.repositories.notes

import db.repositories.DIO
import db.storages.notes.NotesStorage
import slick.dbio.Effect
import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery

abstract class NotesRepository[A, B <: NotesStorage[A]](cons: Tag => B) {
  val AllNotes: TableQuery[B] = TableQuery(cons)

  def getAllUsersNotes(userName: String): DIO[Seq[A], Effect.Read] = {
    AllNotes.filter(_.ownerName === userName).result
  }

  def getSortedAllUsersNotes(userName: String): DIO[Seq[A], Effect.Read] = {
    AllNotes.filter(_.ownerName === userName).sortBy(_.day.asc).result
  }

  def getUserNotesInSpecifiedDay(userName: String, day: Long): DIO[Seq[A], Effect.Read] = {
    AllNotes.filter(rep => rep.ownerName === userName && rep.day === day).result
  }

  def addNote(notes: A): DIO[Int, Effect.Write] = {
    AllNotes += notes
  }

  def addNotes(notes: List[A]): DIO[Option[Int], Effect.Write] = {
    AllNotes ++= notes
  }

  def deleteAll(): DIO[Int, Effect.Write] = {
    AllNotes.delete
  }
}
