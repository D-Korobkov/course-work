package db

import slick.dbio
import slick.dbio.{DBIOAction, NoStream}

package object repositories {
  type DIO[+A, -B <: dbio.Effect] = DBIOAction[A, NoStream, B]
}
