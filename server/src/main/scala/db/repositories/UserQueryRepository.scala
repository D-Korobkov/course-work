package db.repositories

import db.entities.User
import db.storages.UserStorage
import slick.dbio.Effect
import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery

import scala.concurrent.ExecutionContext

object UserQueryRepository {
  val AllUsers = TableQuery[UserStorage]

  def getUserPassword(username: String): DIO[Option[String], Effect.Read] = {
    AllUsers.filter(_.userName === username).map(_.password).result.headOption
  }

  def getUserCodeforcesTokenId(username: String): DIO[Option[Int], Effect.Read] = {
    AllUsers.filter(_.userName === username).map(_.codeforcesTokenId).result.head
  }

  def getUserGithubTokenId(username: String): DIO[Option[Int], Effect.Read] = {
    AllUsers.filter(_.userName === username).map(_.githubTokenId).result.head
  }

  def getUserStravaTokenId(username: String): DIO[Option[Int], Effect.Read] = {
    AllUsers.filter(_.userName === username).map(_.stravaTokenId).result.head
  }

  def getUserCustomCategories(userName: String)(implicit ex: ExecutionContext): DIO[Seq[String], Effect.Read] = {
    AllUsers.filter(_.userName === userName).map(_.customCategories).result.head.map(_.split(";").toSeq)
  }

  def getUserCategories(userName: String)(implicit ex: ExecutionContext): DIO[(Seq[String], Boolean, Boolean, Boolean), Effect.Read] = {
    AllUsers
      .filter(_.userName === userName)
      .map(rep => (rep.customCategories, rep.codeforcesTokenId, rep.githubTokenId, rep.stravaTokenId))
      .result
      .head
      .map {
        case (customCats, cf, gh, strava) => (customCats.split(";").toSeq, cf.isDefined, gh.isDefined, strava.isDefined)
      }
  }

  def findUserByUserName(userName: String): DIO[Option[User], Effect.Read] = {
    AllUsers.filter(_.userName === userName).result.headOption
  }

  def addNewUser(user: User): DIO[Int, Effect.Write] = {
    AllUsers += user
  }

  def updateCustomCategories(userName: String, categories: Seq[String]): DIO[Int, Effect.Write] = {
    AllUsers.filter(_.userName === userName).map(_.customCategories).update(categories.mkString(";"))
  }

  def updateCodeforcesTokenId(userName: String, newTokenId: Option[Int]): DIO[Int, Effect.Write] = {
    AllUsers.filter(_.userName === userName).map(_.codeforcesTokenId).update(newTokenId)
  }

  def updateGithubTokenId(userName: String, newTokenId: Option[Int]): DIO[Int, Effect.Write] = {
    AllUsers.filter(_.userName === userName).map(_.githubTokenId).update(newTokenId)
  }

  def updateStravaTokenId(userName: String, newTokenId: Option[Int]): DIO[Int, Effect.Write] = {
    AllUsers.filter(_.userName === userName).map(_.stravaTokenId).update(newTokenId)
  }
}