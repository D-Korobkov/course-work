package db.repositories.tokens

import db.entities.tokens.LogLifeToken
import db.repositories.DIO
import db.storages.tokens.LogLifeTokenStorage
import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery

import scala.concurrent.ExecutionContext

object LogLifeQueryRepository {
  val AllTokens = TableQuery[LogLifeTokenStorage]

  def getToken(token: String): DIO[Option[LogLifeToken], Effect.Read] = {
    AllTokens.filter(_.token === token).result.headOption
  }

  def addToken(token: LogLifeToken): DIO[Int, Effect.Write] = {
    AllTokens += token
  }

  def deleteToken(token: String): DIO[Int, Effect.Write] = {
    AllTokens.filter(_.token === token).delete
  }

  def deleteOldTokens(lessThan: Long): DIO[Int, Effect.Write] = {
    AllTokens.filter(_.expiresAt <= lessThan).delete
  }
}
