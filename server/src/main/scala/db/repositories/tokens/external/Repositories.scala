package db.repositories.tokens.external

import api.external.github.entities.GithubUserToken
import api.external.strava.entities.StravaUserToken
import db.entities.tokens.{CodeforcesToken, GithubToken, StravaToken}
import db.repositories.DIO
import db.storages.tokens.external.{CodeforcesTokenStorage, GithubTokenStorage, StravaTokenStorage}
import slick.jdbc.H2Profile.api._

// codeforces
object CodeforcesQueryRepository extends TokensRepository[CodeforcesTokenStorage]((tag: Tag) => new CodeforcesTokenStorage(tag)) {
  def addCodeforcesHandle(handle: String): DIO[Int, Effect.Write] = {
    AllTokens += CodeforcesToken(None, handle)
  }
}

// github
object GithubQueryRepository extends TokensRepository[GithubTokenStorage]((tag: Tag) => new GithubTokenStorage(tag)) {
  def addGithubUserToken(githubUserToken: GithubUserToken): DIO[Int, Effect.Write] = {
    AllTokens += GithubToken(None, githubUserToken.token_type, githubUserToken.access_token, githubUserToken.scope)
  }
}

// strava
object StravaQueryRepository extends TokensRepository[StravaTokenStorage]((tag: Tag) => new StravaTokenStorage(tag)) {
  def getRefreshTokenById(id: Int): DIO[Option[String], Effect.Read] = {
    AllTokens.filter(_.tokenId === id).map(_.refreshToken).result.headOption
  }

  def getTokenExpiresAtTime(tokenId: Int): DIO[Option[Long], Effect.Read] = {
    AllTokens.filter(_.tokenId === tokenId).map(_.expiresAt).result.headOption
  }

  def updateToken(tokenId: Int, newToken: StravaUserToken): DIO[Int, Effect.Write] = {
    AllTokens.filter(_.tokenId === tokenId)
      .map(t => (t.accessToken, t.refreshToken, t.expiresIn, t.expiresAt))
      .update((newToken.access_token, newToken.refresh_token, newToken.expires_in, newToken.expires_at))
  }

  def addStravaUserToken(stravaUserToken: StravaUserToken): DIO[Int, Effect.Write] = {
    AllTokens += StravaToken(
      None,
      stravaUserToken.token_type,
      stravaUserToken.access_token,
      stravaUserToken.refresh_token,
      stravaUserToken.expires_in,
      stravaUserToken.expires_at
    )
  }
}