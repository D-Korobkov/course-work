package db.repositories.tokens.external

import db.repositories.DIO
import db.storages.tokens.external.TokenStorage
import slick.jdbc.H2Profile.api._
import slick.lifted.{TableQuery, Tag}

abstract class TokensRepository[B <: TokenStorage[_]](cons: Tag => B)  {
  val AllTokens: TableQuery[B] = TableQuery(cons)

  def getIdByAccessToken(token: String): DIO[Option[Int], Effect.Read] = {
    AllTokens.filter(_.accessToken === token).map(_.tokenId).result.headOption
  }

  def getAccessTokenById(id: Int): DIO[Option[String], Effect.Read] = {
    AllTokens.filter(_.tokenId === id).map(_.accessToken).result.headOption
  }

  def getLastUpdatedByToken(token: String): DIO[Option[Long], Effect.Read] = {
    AllTokens.filter(_.accessToken === token).map(_.lastUpdated).result.headOption
  }

  def updateLastUpdatedByToken(token: String, timeStamp: Long): DIO[Int, Effect.Write] = {
    AllTokens.filter(_.accessToken === token).map(_.lastUpdated).update(timeStamp)
  }
}
