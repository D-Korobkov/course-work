name := "course-work"

version := "0.1"

ThisBuild / scalaVersion := "2.13.4"

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

lazy val circeVersion = "0.13.0"

ThisBuild / libraryDependencies ++= Seq(
  "de.heikoseeberger" %% "akka-http-circe" % "1.36.0",

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,

  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.slf4j" % "slf4j-api" % "1.7.25",
  "ch.qos.logback" % "logback-classic" % "1.2.3",

  "com.typesafe.akka" %% "akka-cluster-typed" % "2.6.13",

  "com.softwaremill.macwire" %% "macros" % "2.3.7",
  "com.beachape" %% "enumeratum" % "1.6.1",
  "com.beachape" %% "enumeratum-circe" % "1.6.1",

  "com.typesafe.slick" %% "slick" % "3.3.3",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
  "org.xerial" % "sqlite-jdbc" % "3.30.1",
  "com.h2database" % "h2" % "1.4.200",
  "org.flywaydb" % "flyway-core" % "7.9.0",

  "com.lightbend.akka" %% "akka-stream-alpakka-slick" % "2.0.2",
  "com.typesafe.akka" %% "akka-stream" % "2.5.31",

  "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.13" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % "10.2.4" % Test,
  "org.scalamock" %% "scalamock" % "4.4.0" % Test,
  "org.scalatest" %% "scalatest" % "3.2.2" % Test,

  "com.github.t3hnar" %% "scala-bcrypt" % "4.3.0",
  "com.github.jwt-scala" %% "jwt-circe" % "7.1.4",
  "com.github.tototoshi" %% "scala-csv" % "1.3.7"
)

lazy val server = project
