"use strict"

let unixTime = undefined
let diaryPage = 1

let parseStandardInfo = notes => {
    let div = $(".standard")
    div.empty()
    notes.map(note => standardCats.map(category => {
            let value = note[category]
            if (value !== null && (category === "awakenTime" || category === "asleepTime")) {
                value = new Date(value * 1000).toLocaleTimeString().slice(0, 5)
            } else if (value === null) {
                value = ""
            }
            div.append("<div class='note'><h4>" + category + ":</h4><p>" + value + "</p></div>")
        })
    )
}

let parseCustomInfo = notes => {
    let div = $(".custom")
    div.empty()
    notes.map(note => {
        div.append("<div class='note'><h4>" + note["categoryName"] + ":</h4><p>" + note["text"] + "</p></div>")

    })
}

let parseCodeforcesInfo = notes => {
    let div = $(".codeforces")
    div.empty()
    notes.map(note =>
        div.append("<div class='note'><h4>rating changes:</h4><p>" + note["ratingChange"] + "</p></div>")
            .append("<div class='note'><h4>solved tasks:</h4><p>" + note["solvedTasks"] + "</p></div>")
    )

}

let parseGithubInfo = notes => {
    let div = $(".github")
    div.empty()
    notes.map(note =>
        div.append("<div class='note'><h4>" + note["repositoryName"] + ":</h4><p>" + note["numberOfCommits"] + "</p></div>")
    )

}

let parseStravaInfo = notes => {
    let div = $(".strava")
    div.empty()
    notes.map(note =>
        div.append("<div class='note'><h4>" + note["activityType"] + ":</h4><p>" + note["distance"] + " meters</p></div>")
    )
}

let parsePageInfo = request => {
    let info = JSON.parse(request.responseText)
    parseStandardInfo(info["standardNotes"])
    parseCustomInfo(info["customNotes"])
    parseCodeforcesInfo(info["codeforcesNotes"])
    parseGithubInfo(info["githubNotes"])
    parseStravaInfo(info["stravaNotes"])
}

let updatePageInfo = () => {
    let dayWhenPageWasCreated = unixTime - secondsInDay * diaryPage
    $("#day").empty().append(new Date(dayWhenPageWasCreated * 1000).toLocaleString())
    buildRequestWithDefaultErrorHandler(
        "GET",
        "/auth/diary/get/" + dayWhenPageWasCreated,
        request => {
            console.log(request.responseText);
            parsePageInfo(request)
        }
    ).send()
}

let addPrevNextButtonEvent = () => {
    $("#load-previous-sheet").bind("click", () => {
        diaryPage += 1
        updatePageInfo()
    })
    $("#load-next-sheet").bind("click", () => {
        if (diaryPage > 1) {
            diaryPage -= 1
            updatePageInfo()
        } else {
            alert("Время ещё не пришло")
        }
    })
}

window.onload = () => {
    verifyToken()
    getAllUsersCategories().then(addPrevNextButtonEvent)
    unixTime = countTheEndOfThisDayInUnixFormat()
    updatePageInfo()
}