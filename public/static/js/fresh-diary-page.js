"use strict"

let getCheckedValue = cat => $("#" + cat).find("input:checked").val()

let saveStandardFunction = () => {
    let message = {}
    standardCats.map(cat => {
        if (cat === "mood" || cat === "productivity") {
            let rate = getCheckedValue(cat)
            if (rate !== undefined) {
                message[cat] = parseInt(rate)
            }
        } else if (cat === "asleepTime" || cat === "awakenTime") {
            let hoursAndMinutes = $("#" + cat).val()
            if (hoursAndMinutes !== "") {
                let offset = new Date().getTimezoneOffset()
                let localTime = parseInt(hoursAndMinutes.substr(0, 2)) * 60 + parseInt(hoursAndMinutes.substr(3, 2))
                message[cat] = countTheEndOfThisDayInUnixFormat() - secondsInDay + (localTime + offset) * 60
            }
        } else {
            message[cat] = parseInt($("#" + cat).val())
        }
    })
    let request = buildRequestWithDefaultErrorHandler(
        "POST",
        "/auth/standard/add",
        () => alert("saved")
    )
    request.setRequestHeader("Content-Type", "application/json")
    request.send(JSON.stringify(message))
}

let saveCustomFunction = () => {
    customCats.map(cat => {
        let request = buildRequest(
            "POST",
            "/auth/custom/addNote",
            () => alert("saved")
        )
        request.setRequestHeader("Content-Type", "application/json")
        request.send(JSON.stringify({"categoryName": cat, "text": document.getElementById(cat).value}))
    })
}

let getPreviouslyAddedStandardInfo = () => {
    buildRequest(
        "GET",
        "/auth/standard/get/" + countTheEndOfThisDayInUnixFormat(),
        request => {
            let info = JSON.parse(request.responseText)
            standardCats.map(cat => {
                if (info[cat] !== null) {
                    if (cat === "mood" || cat === "productivity") {
                        $("#" + cat).find("input[value='" + info[cat] + "']").attr("checked", "true")
                    } else if (cat === "awakenTime" || cat === "asleepTime") {
                        $("#" + cat).val(new Date(info[cat] * 1000).toLocaleTimeString().slice(0, 5))
                    } else {
                        $("#" + cat).val(info[cat])
                    }
                }
            })
        }
    ).send()
}

let getPreviouslyAddedCustomInfo = () => {
    if (customCats.length > 0 && customCats[0].length > 0) {
        let custom = $("#custom")
        customCats.map(cat => {
                custom.append("<textarea class='big-field' id='" + cat + "' placeholder='" + cat + "'></textarea>")
                buildRequest(
                    "GET",
                    "/auth/custom/get/" + cat + "/" + countTheEndOfThisDayInUnixFormat(),
                    request => {
                        let info = JSON.parse(request.responseText)
                        if (info && info["text"]) {
                            document.getElementById(cat).value = info["text"]
                        }
                    }
                ).send()
            }
        )
        custom.append("<div class='buttons-container'><button id='save-custom'>save</button></div>")
        $("#save-custom").bind("click", saveCustomFunction)
    }
}

let getPreviouslyAddedInfo = () => {
    getPreviouslyAddedStandardInfo()
    getPreviouslyAddedCustomInfo()
}

window.onload = () => {
    verifyToken()
    getAllUsersCategories().then(getPreviouslyAddedInfo)
    $("#back").bind("click", () => location.replace("settings"))
    $("#save-standard").bind("click", saveStandardFunction)
}