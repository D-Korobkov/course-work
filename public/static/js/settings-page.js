"use strict"

let addCustomCategory = () => {
    let catName = prompt("Введите название вашей собственной категории: ", undefined)
    if (catName !== undefined && catName !== null && catName !== "") {
        buildRequestWithDefaultErrorHandler(
            "POST",
            "/auth/custom/add/" + catName,
            () => $("#custom-categories").append("<li>" + catName + "</li>")
        ).send()
    }
}

let initCodeforces = () => {
    let nickname = prompt("Введите ваш handle на Codeforces: ", undefined)
    if (nickname !== undefined && nickname !== null && nickname !== "") {
        buildRequestWithDefaultErrorHandler(
            "POST",
            "/auth/codeforces/init?nickname=" + nickname,
            () => location.reload()
        ).send()
    }
}

let initGithub = () => {
    buildRequestWithDefaultErrorHandler(
        "GET",
        "/auth/github/init",
        request => location.replace(request.responseText)
    ).send()
}

let initStrava = () => {
    buildRequestWithDefaultErrorHandler(
        "GET",
        "/auth/strava/init",
        request => location.replace(request.responseText)
    ).send()
}

let updateService = service => () => {
    buildRequestWithDefaultErrorHandler(
        "POST",
        "/auth/" + service + "/update",
        () => alert("Обновление профиля может занять некоторое время")
    ).send()
}

let getDownloadLink = () => {
    buildRequestWithDefaultErrorHandler(
        "POST",
        "/auth/diary/download",
        request => downloadFile("/auth/" + request.responseText)
    ).send()
}

let downloadFile = uri => {
    buildRequestWithDefaultErrorHandler(
        "GET",
        uri,
        request => {
            let link = document.createElement("a")
            let url = URL.createObjectURL(new Blob(["\ufeff", request.response]))
            link.setAttribute("href", url)
            link.setAttribute("download", "diary.csv")
            link.click()
        }
    ).send()
}

const addButtonsEvents = () => {
    $("#download").bind("click", () => getDownloadLink())
    $("#new").bind("click", () => location.replace("new"))
    $("#back").bind("click", () => location.replace("personal"))
    $("#log-out").bind("click", () => {
        deleteCookie("token")
        location.replace("../")
    })
    $("#init-codeforces").bind("click", initCodeforces)
    $("#init-github").bind("click", initGithub)
    $("#init-strava").bind("click", initStrava)
    $("#add-custom").bind("click", addCustomCategory)
}

const setAvailableCategories = () => {
    if (customCats.length > 0 && customCats[0] !== "") {
        customCats.reverse().map(category => $("#custom-categories").append("<li>" + category + "</li>"))
    }
    autoCats.map(category => {
        $("#" + category).replaceWith("<h3 class='supported'>OK</h3>")
        $("#init-" + category).replaceWith("<button id='update-" + category + "'>update</button>")
    })
    autoCats.map(category => $("#update-" + category).bind("click", updateService(category)))
}

window.onload = () => {
    verifyToken()
    getAllUsersCategories().then(setAvailableCategories)
    addButtonsEvents()
}