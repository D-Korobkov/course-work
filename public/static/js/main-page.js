"use strict"

let readValue = id => () => document.getElementById(id).value

let simpleAuthFunction = (username, password, successHandler) => url => {
    let request = buildRequestWithDefaultErrorHandler(
        "POST",
        url,
        request => successHandler(request)
    )
    request.setRequestHeader("Content-Type", "application/json")
    request.send(JSON.stringify({"username": username, "password": password}))
}

let signInFunction = (getLogin, getPassword) => () => {
    simpleAuthFunction(getLogin(), getPassword(), request => {
        setCookie("token", request.getResponseHeader("Authorization"))
        location.replace("/site/personal")
    })("signIn")
}

let signUpFunction = (getLogin, getPassword) => () => {
    let login = getLogin()
    let password = getPassword()
    simpleAuthFunction(login, password, signInFunction(() => login, () => password))("signUp")
}

let initDocument = () => {
    if (accessToken !== undefined) {
        location.replace("/site/personal")
    }
    document
        .getElementById("sign-in")
        .addEventListener("click", signInFunction(readValue("login"), readValue("password")))
    document
        .getElementById("sign-up")
        .addEventListener("click", signUpFunction(readValue("login"), readValue("password")))
}

window.onload = initDocument