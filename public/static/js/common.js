"use strict"

const secondsInDay = 86400

let standardCats = undefined
let customCats = undefined
let autoCats = []

let accessToken = undefined
if (document.cookie.indexOf('token=') !== -1) {
    accessToken = getCookie("token")
}

let countTheEndOfThisDayInUnixFormat = () => {
    let now = Math.floor(new Date().getTime() / 1000)
    return now - now % secondsInDay + secondsInDay
}

let buildRequest = (method, uri, successFunction, errorFunction) => {
    let request = new XMLHttpRequest()
    request.open(method, uri, true)
    request.setRequestHeader("Authorization", accessToken)
    request.onload = () => {
        if (request.status !== 200) {
            errorFunction(request)
        } else {
            successFunction(request)
        }
    }
    return request
}

let buildRequestWithDefaultErrorHandler = (method, uri, successFunction) => buildRequest(
    method, uri, successFunction, request => alert(JSON.parse(request.responseText)["errorMessage"])
)

let verifyToken = () => {
    if (accessToken !== undefined) {
        buildRequest(
            "GET",
            "/auth",
            request => $("#personality").append(("<div class='name'><h1>" + request.responseText + "</h1> </div>")),
            () => location.replace("../")
        ).send()
    } else {
        location.replace("../")
    }
}

let getAllUsersCategories = () => new Promise(resolver => {
        buildRequestWithDefaultErrorHandler(
            "GET",
            "/auth/categories",
            request => resolver(request)// => parseCategories(JSON.parse(request.responseText))
        ).send()
    }
).then(request => parseCategories(JSON.parse(request.responseText)))

let parseCategories = response => {
    standardCats = response["standard"]
    customCats = response["custom"]
    checkAutoCategoriesAccess(response, "codeforces")
    checkAutoCategoriesAccess(response, "github")
    checkAutoCategoriesAccess(response, "strava")
}

let checkAutoCategoriesAccess = (jsonArr, catName) => {
    if (jsonArr[catName]) {
        autoCats.push(catName)
    }
}
